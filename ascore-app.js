/* official ascore ng-app */

var ascoreApp = angular.module('ascoreApp', ['ascoreControllers','ascoreServices','ascoreDirectives','ascoreFilters','flow','angularFileUpload','ngSanitize','validation','validation.rule']);

/* NG-CONTROLLER */

var ascoreControllers = angular.module('ascoreControllers',[]);

ascoreControllers.controller('HeadController', ['$scope','panel', function($scope, panel){

	$(document).ready(function(){
		
    $('[data-toggle="tooltip"]').tooltip();
		
		$(".inline-popup .target").parent().click(function(e){
			
			e.stopPropagation();
			
			if($(this).is(e.target)){$('.inline-popup').hide();}
		});
		
		$(".inline-popup .close").click(function(){
			$('.inline-popup').hide();
		});
		
		$(document).keyup(function(e){
			
			if(e.which == 27){$('.inline-popup').hide();}
		});
	});
	
	$scope.$watch(function(){
	
		$scope.title = panel.getPageTitle();
		
		$scope.styles = panel.getCssStyle();
		
		$scope.scripts = panel.getScriptSrc();
	});
}]);

ascoreControllers.controller('MailController', ['$scope','panel', '$compile', function($scope, panel, $compile){

	$scope.init = function(bind){
	
        $scope.subjectLength = 80;
 
        $scope.remainLength = angular.copy($scope.subjectLength);
        
		if(bind){
			
			var data = panel.data(bind);
		}
		else{
			
			var data = panel.data();
		}
		
		for(var i in data){
		
			$scope[i] = data[i];
		}
		
		$scope.response = {};
		
		$scope.actionList = [];
		
		$(document).ready(function(){
			
			if($("#init-activity")){
				
				$('#ajax-view-display').css({'z-index':2});
				
				$('.profile-view.other').show();
				
				$('.profile-view.basic').hide();
				
				$scope.interact.register_click_event();
				
				$("#mobile-title").text('Messages');
			}
		});
	}
	
	
	$scope.interact = {
		
		register_click_event: function(){
			
			$('#rewinder').unbind("click");
			
			$('#rewinder').click(function(){$scope.interact.rewind();});
		},
		
		ajax: function(page, params){
			
			$scope.interact.register_click_event();
			
			var parameters = '&';
			
			if(params){for(var i in params){parameters += i+'='+params[i]+'&';}}
			
			var url = '/user/'+$scope.user.username+'/'+page+'?json=true&ajax=true'+parameters;
			
			panel.fetch(url, function(response){
				
				panel.loading(false);
				
				$('body, html').animate({scrollTop: 0}, 500);
				
				var content = $(response);
				
				$compile(content)($scope);
				
				$('#ajax-viewer').html(content);
				
				$('#ajax-view-display').css({'z-index':2});
				
				$('.profile-view.other').show();
				
				$('.profile-view.basic').hide();
				
				$scope.init('#init-activity');
				
				$(document).ready(function(){$("#mobile-title").html($scope.mobile_title);});
				
			}, true, true);
		},
		
		rewind: function(){
			
			$('#ajax-viewer').html('');
				
			$('#ajax-view-display').css({'z-index':-100});
			
			$('.profile-view.other').hide();
			
			$('.profile-view.basic').show();
			
			window.history.back();
		}
	}
	
	$scope.check_all = function(bool){
		
		var option = $scope.outbox ? 'outbox' : 'mail';
		
		for(var i in $scope[option]){
				
			$scope[option][i].selected = bool;
		}
		
		if(bool){
			
			$scope.actionList = $scope[option].slice();
			
			$('.check-all').addClass('disabled');
		}
		else{
			
			$scope.actionList = [];
			
			$('.check-all').removeClass('disabled');
		}
	}
	
	$scope.toActionList = function(message){
		
		var i = $scope.actionList.indexOf(message);
		
		if(message.selected){
			
			$scope.actionList.push(message);
		}
		else{
			
			$scope.actionList.splice(i, 1);
		}
	}
	
	$scope.actions = {
		
		expand: function(m){
			
			var read = $("#read-"+m.id), outbox = $("#outbox-"+m.id);
			
			outbox.slideToggle(200);
		},
		
		'delete': function(thread_id){
			
			
			panel.confirm("Are you sure you want to delete the selected messages?", function(){
				
				var option = $scope.outbox ? 'outbox' : 'mail';
				
				var threads = [];
				
				if(thread_id){
					
					threads.push(thread_id);
				}
				else{
					
					if(option == 'mail'){for(var i in $scope.actionList){threads.push($scope.actionList[i].thread_id);}}
					else{for(var i in $scope.actionList){threads.push($scope.actionList[i].id);}}
				}
				
				
				if(threads.length > 0){
					
					
					
					var data = {action: 'delete', list: threads, scope: option, business_id:$scope.business_id};
					
					panel.post('/messages/action', data, function(response){
						if(response.Request == 'OK'){
										
							if($scope.reader){
								
								delete $scope.reader;
								
								for(var i in $scope.mail){
									
									if($scope.mail[i].thread_id == thread_id){
										
										$scope.mail.splice(i, 1); break;
									}
								}
								
								$scope.actions.back_to_inbox('reader');
							}
							else{
								
								for(var i in $scope.actionList){
								
									var j = $scope[option].indexOf($scope.actionList[i]);
									
									$scope[option].splice(j, 1);
								}
							
								$scope.actionList.splice(0, $scope.actionList.length);
							}
						}
					});
				}
			});
		},
		
		back_to_inbox: function(item){
			
			delete $scope[item];
			
			$('.view-thread').addClass('hide');
		},
		
		compose: function(){
			
			$scope.composer = {subject:'', message:''};
		},
		
		delete_chat: function(chat){
			
			
		},
		
		report_chat: function(chat){
			
			
		},
		
		block_user: function(chat){
			
			
		}
	}
	
	$scope.searchRecepients = function(){
		
		$("#recepients-search").autocomplete({
			
			source: function(request, response){
				$.ajax({
					url: '/messages/search',
					data:{
						'query':request.term,
					}
				}).success(function(data){
					
					var list = $.parseJSON(data);
					
					response($.map(list, function(info){
						
						var x = {
						'value' : info.name,
						'label' : info.name,
						}
						
						for(i in info){x[i] = info[i];}
						
						return x;
					}));
				});
			},
			
			select: function(event, ui){
				
				if(!$scope.recepients){$scope.recepients = [];}
				
				$scope.recepients[0] = ui.item;
			}
		});
	}
	
	$scope.send = function(){
		
		var composer = $scope.composer;
		
		if(!composer.subject || composer.subject.trim().length == 0){
			
			panel.alert("Please enter a subject");return;
		}
		
		if(!composer.message || composer.message.trim().length == 0){
			
			panel.alert("Please enter a message");return;
		}
		
		if(!$scope.recepients || $scope.recepients.length == 0){
			
			panel.alert("Please enter a recepient from the search dropdown!");return;
		}
		
		var size = $scope.recepients.length;
		
		for(var i in $scope.recepients){
			
			var recepient = $scope.recepients[i];
			
			var mail = {type:'mail', 'scope':recepient.scope, receipient_id: recepient.id, subject:composer.subject, message:composer.message};
			
			panel.post('/incoming', mail, function(response){
							
				if((response.Request) == 'OK'){
					
					size = size - 1;
					
					if(size == 0){
						
						panel.alert('Message send!');
						
						delete $scope.composer;
					}
				}
			});
		}
	}
	
	$scope.read = function(message, isBusiness){
		
		var data = {id:message.id, read:message.read, thread_id:message.thread_id, mail_id:message.mail_id};
		
		if(isBusiness){data.business_id=$scope.business_id}
		
		panel.post('/messages/thread', data, function(response){
			
			if(!$scope.__recepient__){$scope.__recepient__ = 'U';}
			
			$scope.reader = message;
			
			$scope.thread = response;
			
			var i = $scope.mail.indexOf(message);
			
			$scope.mail[i].read = 1;
			
			$(document).ready(function(){
				
				div = $(".view-thread");
				
				div.removeClass('hide');
				
				$('html,body').animate({scrollTop: div.offset().top},'fast');
				
				div.scrollTop(div.prop("scrollHeight"));
			});
			
		});
	}
	
	/*if we detect just one business then recepient its a business*/
	$scope.set_recepient_type = function(t){if(t.scope == 'B'){$scope.__recepient__ = 'B';}}
	
	$scope.reply = function(isBusiness){
	
		if($scope.response.message.trim().length > 0){
			
			$scope.response.type = 'mail';
			
			$scope.response.rtype = $scope.__recepient__;
			
			$scope.response.receipient_id = $scope.reader['sender_id'];
			
			$scope.response.thread_id = $scope.reader['thread_id'];
			
			$scope.response.subject = $scope.reader['subject'];
			
			$scope.response.is_reply = 1;
			
			$scope.response.scope = !isBusiness ? 'U' : 'B';
			
			if(isBusiness || $scope.isBusiness){$scope.response.business_id = $scope.business_id;}
			
			panel.post('/incoming', $scope.response, function(response){
				
				if((response.Request) == 'OK'){
					
					$scope.response = {};
					
					$scope.thread.push(response.message);
				}
			});
		}
	}
	
	$scope.toggle_preview = function($event, id){
	
		var tp = $("#preview_"+id);
		
		var ttp = $("#t_preview_"+id);
		
		if(!tp.hasClass('exposed')){
			
			tp.addClass('exposed');
			
			tp.show();
			
			ttp.hide();
			
			$($event.target).removeClass('toggleDown');
			
			$($event.target).addClass('toggleUp');
		}
		else{
			
			tp.removeClass('exposed');
			
			tp.hide();
			
			ttp.show();
			
			$($event.target).removeClass('toggleUp');
			
			$($event.target).addClass('toggleDown');
		}
	}
    
    $scope.calRemain = function() {
 
        $scope.remainLength = $scope.subjectLength - $scope.composer.subject.length;
 
    }
 
	
}]);

ascoreControllers.controller('BusinessController', ['$scope','panel', 'FileUploader', function($scope, panel, FileUploader){
	
	$scope.mobile = {
		
		fetch_reviews: function(){
			
			$scope.fetched_reviews = false;
			
			panel.fetch('/review?business='+$scope.biz.id+'&page=1&json=1', function(response){
				
				if(response.Request == 'OK'){
					
					$scope.fetched_reviews = true;
					
					$scope.reviews = response.reviews;
				}
			});
		}
	}
	
	$scope.demoFeatures = function(){
		
		var demoKeys = ["Takes Reservations","Delivery","Take-out","Accepts Credit Cards","Good For","Parking","Bike Parking","Wheelchair Accessible","Good for Kids","Good for Groups","Attire","Ambience","Noise Level","Music","Good For Dancing","Alcohol","Happy Hour","Best Nights","Coat Check","Smoking","Outdoor Seating","WiFl","Has TV","Waiter Service","Caters","Has Pool Table"];
		var demoValues = ["No","No","No","No","Dlnner","Street","Yes","No","No","Yes","Casual","Trendy","Average","Background","No","Full Bar","No","Thu. Fri. Sat","No","No","Yes","No","No","Yes","No","No"];
		
		
		$scope.demoFeat = [];
		
		for(var i = 0; i < demoKeys.length ; i++){
			
			var o = {key:demoKeys[i], value:demoValues[i]};
			
			$scope.demoFeat.push(o);
		}
	}
	
	$scope.demoFeatures();
	
	$scope.init = function(){
		
		var data = panel.data();
		
		for(var i in data){$scope[i] = data[i];}
		
		if(typeof $scope.address == 'undefined'){
			
			$scope.address = '';
			
			var address_sections = ['street_line_1','street_line_2','postal_code','city','state','country'];
			
			for(var i in address_sections){
				
				var section = $scope.biz[address_sections[i]];
				
				if( section != null){
					
					$scope.address = $scope.address + section+', ';
				}
			}
			
			$scope.address = $scope.address.substring(0, $scope.address.length - 2);
		}
		
		$(document).ready(function(){
			
			var PURPOSE_FOR_THIS_BLOCK = "hightlight active parts of document";
			
			$('.'+$scope.reviews_showing).addClass('light-grey');
			
			$scope.num_reviews = (function(){if($scope.reviews_showing == 'video'){return $scope.stats.reviewCountVideo;}else if($scope.reviews_showing == 'text'){return $scope.stats.reviewCount - $scope.stats.reviewCountVideo;} else{ return $scope.stats.reviewCount;} })();
			
			var max_pages = Math.ceil($scope.num_reviews / $scope.per_page);
			
			$scope.pagelist = [];
			
			for(var i = 0; i < max_pages; i++){$scope.pagelist.push(i+1);}
			
			if($scope.page_no < max_pages){$scope.nextPage = parseInt($scope.page_no)+1;}
			
			$scope.showing_from = 1;
				
			$scope.showing_to = $scope.reviews.length;
			
			if($scope.page_no > 1){
				
				$scope.prevPage = parseInt($scope.page_no)-1;
				
				$scope.showing_from = ($scope.page_no*$scope.per_page)-$scope.per_page+1;
				
				$scope.showing_to = $scope.showing_from+$scope.reviews.length-1;
			}
		});
	}
	
	$scope.init();
	
	$scope.set_selected_page = function(page){if($scope.page_no == page){return 'selected';}}
	
	$scope.get_address = function(){
		
		return $scope.address;
	}
	
	$scope.paragraph = function(str){
		
		return str.replace(/(?:\r\n|\r|\n)/g, '<br>');
	}
	
	$scope.url_string = function(str){
	
		return str.replace('&', "and").replace(/\s/g, "-").toLowerCase();
	}
	
	$scope.is_today = function(day){
			
		var d = new Date(), n = d.getDay();
		
		var _class = (day.open_day == n) ? 'bold black' : '';
		
		if(_class.length > 0){
			
			var h = d.getHours();
			
			$scope.is_open_now = (day.oh <= h && h < day.ch) ? 'Open Now' : 'Closed';
		}
		
		return _class;
	}
	
	$scope.gallery_loader = function(callback){
		
		var data = {'business_id':$scope.biz.id};
		
		panel.post('/business/gallery', data, function(response){
			
			(callback)(response.gallery);
		}, true);
	}
	
	$scope.gallery = function(business_id, review_id, photo){
		
		var data = {'business_id':business_id, 'review_id':review_id, 'photo':photo};
		
		panel.post('/business/gallery', data, function(response){
			
			var pswpElement = document.querySelectorAll('.pswp')[0];
		
			/* build items array*/
			var items = [];
			
			for(var i in response.gallery){
				
				items.push(
					{
						src: panel.__staticserv()+'/images/'+response.gallery[i]['filename'],
						w: response.gallery[i]['width'],
						h: response.gallery[i]['height']
					}
				);
			}
			
			/* define options (if needed)*/
			var options = {
				/* optionName: 'option value'
				 for example:*/
				index: 0, /* start at first slide */
				showHideOpacity:true,
				imageScaleMethod:  "fitNoUpscale",
			};
			
			/* Initializes and opens  */
			var gallery = new PhotoSwipe( pswpElement, PhotoSwipeUI_Default, items, options);
			
			gallery.init();
		});
	}
	
	$scope.init_photo_uploader = function(){
		
		var props = {
			
				autoUpload:true,
				
				url:'/upload-business-photos/'+$scope.biz.id,
				
				queueLimit:10,
				
				removeAfterUpload :true
			};
		
		$scope.uploader = new FileUploader(props);
		
		$scope.uploader.onBeforeUploadItem  = function(item){
			
			
		}
		
		$scope.uploader.onSuccessItem = function(item, response, status, headers){
			
			if(response.Request == 'OK'){
				
				console.log('Upload success!!!');
			}
		}
		
		$scope.uploader.onCompleteAll = function(){
			
			location.href = location.href+'?page=add-captions';
		}
		
		$scope.click_uploader = function(){$("#uploader").click();}
	
		$scope.upload_photos = function(){
			
			panel.continueIfAccess(function(){$scope.uploader.uploadAll();});
		}
		
		
		$scope.cancel_photos = function(){$scope.uploader.cancelAll();$scope.uploader.clearQueue();}
	}
	
	$scope.init_photo_caption = function(){
		
		$scope.load_caption = function(photo){
			
			$('.caption').removeClass('hightlighted');
			
			$('#photo-'+photo.id).addClass('hightlighted');
			
			var i = $scope.photos.indexOf(photo);
			
			$scope.indexOfCaption = i;
			
			$scope.current_caption = $scope.photos[i];
			
			$("#delete-photo").prop('disabled', false);
			
			$("#edit-caption").focus();
		}
		
		$scope.save_caption = function(){
			
			$scope.photos[$scope.indexOfCaption].caption = $scope.current_caption.caption;
		}
		
		$scope.delete_photo = function(){
			
			panel.confirm("Are you sure you want to delete this photo? This cannot be undone!!!", function(){
				
				var data = {q:'delete', id:$scope.current_caption.id, pid:$scope.current_caption.parent_id};
				
				panel.post(location.href, data, function(response){
					
					if(response.Request == 'OK'){
						
						var i = $scope.photos.indexOf($scope.current_caption);
			
						$scope.photos.splice(i, 1);
						
						$("#delete-photo").prop('disabled', true);
			
						panel.alert("Photo deleted!");
					}
				});
			});
		}
		
		$scope.save_captions = function(){
			
			var photos = {q:'captions', photos:$.parseJSON(angular.toJson($scope.photos))};
			
			panel.post(location.href, photos, function(response){
				
				if(response.Request == 'OK'){
					
					location.href = '/upload-business-photos/'+$scope.biz.id+'?page=captions-thanks-you';
				}
			});
		}
	}
	
	$scope.bs = {
		
		prompt_review_type: function(id, name){
			
			$("#review-type-prompt").show();
			
			if($('body').attr('id') == 'handheld'){$("#review-type-prompt").css({top:$(document).scrollTop()+'px'});}
			
			$scope.textReviewLink = '/review/write?stage=2&type=text&id='+id+'&name='+name;
			
			$scope.videoReviewLink = '/review/write?stage=2&type=video&id='+id+'&name='+name;
		},
		
		pop_business_details: function(){
			
			$("#rating-breakdown").show();
			
			if($('body').attr('id') == 'handheld'){$("#rating-breakdown").css({top:$(document).scrollTop()+'px'});}
		},
		
		pop_review_details: function(review){
			
			if(typeof review == 'number'){
				
				for(var i in $scope.reviews){
					
					if($scope.reviews[i]['id'] == review){
						
						review = $scope.reviews[i]; break;
					}
				}
			}
			
			$scope.current_review_q = review.answers;
			
			$scope.current_review_rating = 0;
			
			for(var i in $scope.current_review_q){
				
				$scope.current_review_rating = $scope.current_review_rating + parseInt($scope.current_review_q[i].answer);
			}
			
			$scope.current_review_rating = ($scope.current_review_rating/$scope.current_review_q.length)*10;
			
			$("#review-rating-breakdown").show();
			
			if($('body').attr('id') == 'handheld'){$("#review-rating-breakdown").css({top:$(document).scrollTop()+'px'});}
		}
	}
}]);

ascoreControllers.controller('BizController', ['$scope','panel', 'FileUploader', function($scope, panel, FileUploader){
	
	$scope.demoFeatures = function(){
		
		var demoKeys = ["Takes Reservations","Delivery","Take-out","Accepts Credit Cards","Good For","Parking","Bike Parking","Wheelchair Accessible","Good for Kids","Good for Groups","Attire","Ambience","Noise Level","Music","Good For Dancing","Alcohol","Happy Hour","Best Nights","Coat Check","Smoking","Outdoor Seating","WiFl","Has TV","Waiter Service","Caters","Has Pool Table"];
		var demoValues = ["No","No","No","No","Dlnner","Street","Yes","No","No","Yes","Casual","Trendy","Average","Background","No","Full Bar","No","Thu. Fri. Sat","No","No","Yes","No","No","Yes","No","No"];
		
		
		$scope.demoFeat = [];
		
		for(var i = 0; i < demoKeys.length ; i++){
			
			var o = {key:demoKeys[i], value:demoValues[i]};
			
			$scope.demoFeat.push(o);
		}
	}
	
	$scope.demoFeatures();
	
	$scope.init = function(){
		
		$scope.info = panel.data();
		
		if(typeof $scope.address == 'undefined'){
			
			$scope.address = '';
			
			var address_sections = ['street_line_1','street_line_2','postal_code','city','state','country'];
			
			for(var i in address_sections){
				
				var section = $scope.info.biz.location[address_sections[i]];
				
				if( section != null){
					
					$scope.address = $scope.address + section+', ';
				}
			}
			
			$scope.address = $scope.address.substring(0, $scope.address.length - 2);
		}
		
		$(document).ready(function(){
			
			var PURPOSE_FOR_THIS_BLOCK = "hightlight active parts of document";
			
			$('.'+$scope.reviews_showing).addClass('light-grey');
			
			$scope.num_reviews = (function(){if($scope.reviews_showing == 'video'){return $scope.stats.reviewCountVideo;}else if($scope.reviews_showing == 'text'){return $scope.stats.reviewCount - $scope.stats.reviewCountVideo;} else{ return $scope.stats.reviewCount;} })();
			
			var max_pages = Math.ceil($scope.num_reviews / $scope.per_page);
			
			$scope.pagelist = [];
			
			for(var i = 0; i < max_pages; i++){$scope.pagelist.push(i+1);}
			
			if($scope.page_no < max_pages){$scope.nextPage = parseInt($scope.page_no)+1;}
			
			$scope.showing_from = 1;
				
			$scope.showing_to = $scope.reviews.length;
			
			if($scope.page_no > 1){
				
				$scope.prevPage = parseInt($scope.page_no)-1;
				
				$scope.showing_from = ($scope.page_no*$scope.per_page)-$scope.per_page+1;
				
				$scope.showing_to = $scope.showing_from+$scope.reviews.length-1;
			}
		});
	}
	
	$scope.init();
	
	$scope.set_selected_page = function(page){if($scope.page_no == page){return 'selected';}}
	
	$scope.get_address = function(){
		
		return $scope.address;
	}
	
	$scope.paragraph = function(str){
		
		return str.replace(/(?:\r\n|\r|\n)/g, '<br>');
	}
	
	$scope.url_string = function(str){
	
		return str.replace('&', "and").replace(/\s/g, "-").toLowerCase();
	}
	
	$scope.is_today = function(day){
			
		var d = new Date(), n = d.getDay();
		
		var _class = (day.open_day == n) ? 'bold black' : '';
		
		if(_class.length > 0){
			
			var h = d.getHours();
			
			$scope.is_open_now = (parseInt(day.oh) <= h && h < parseInt(day.ch)) ? 'Open Now' : 'Closed';
		}
		
		return _class;
	}
	
	$scope.gallery = function(business_id, review_id, photo){
		
		var data = {'business_id':business_id, 'review_id':review_id, 'photo':photo};
		
		panel.post('/business/gallery', data, function(response){
			
			var pswpElement = document.querySelectorAll('.pswp')[0];
		
			/* build items array*/
			var items = [];
			
			for(var i in response.gallery){
				
				items.push(
					{
						src: panel.__staticserv()+'/images/'+response.gallery[i]['filename'],
						w: response.gallery[i]['width'],
						h: response.gallery[i]['height']
					}
				);
			}
			
			/* define options (if needed)*/
			var options = {
				/* optionName: 'option value'
				 for example:*/
				index: 0, /* start at first slide */
				showHideOpacity:true,
				imageScaleMethod:  "fitNoUpscale",
			};
			
			/* Initializes and opens  */
			var gallery = new PhotoSwipe( pswpElement, PhotoSwipeUI_Default, items, options);
			
			gallery.init();
		});
	}
	
	$scope.init_photo_uploader = function(){
		
		var props = {
			
				autoUpload:true,
				
				url:'/upload-business-photos/'+$scope.info.biz.about.id,
				
				queueLimit:10,
				
				removeAfterUpload :true
			};
		
		$scope.uploader = new FileUploader(props);
                
                $scope.uploader.filters.push({
                    name: 'imageFilter',
                    fn: function(item /*{File|FileLikeObject}*/, options) {
                    var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
                    return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
                     }
                });
		
		$scope.uploader.onBeforeUploadItem  = function(item){
			
			
		}
		
		$scope.uploader.onSuccessItem = function(item, response, status, headers){
			
			if(response.Request == 'OK'){
				
				console.log('Upload success!!!');
			}
		}
		
		$scope.uploader.onCompleteAll = function(){
			
			location.href = location.href+'?page=add-captions';
		}
		
		$scope.click_uploader = function(){$("#uploader").click();}
	
		$scope.upload_photos = function(){
			
			panel.continueIfAccess(function(){$scope.uploader.uploadAll();});
		}
		
		
		$scope.cancel_photos = function(){
                    $scope.uploader.cancelAll();
                    $scope.uploader.clearQueue();
                    window.history.back();
                }
	}
	
	$scope.init_photo_caption = function(){
		
		$scope.load_caption = function(photo){
			
			$('.caption').removeClass('hightlighted');
			
			$('#photo-'+photo.id).addClass('hightlighted');
			
			var i = $scope.photos.indexOf(photo);
			
			$scope.indexOfCaption = i;
			
			$scope.current_caption = $scope.photos[i];
			
			$("#delete-photo").prop('disabled', false);
			
			$("#edit-caption").focus();
		}
		
		$scope.save_caption = function(){
			
			$scope.photos[$scope.indexOfCaption].caption = $scope.current_caption.caption;
		}
		
		$scope.delete_photo = function(){
			
			panel.confirm("Are you sure you want to delete this photo? This cannot be undone!!!", function(){
				
				var data = {q:'delete', id:$scope.current_caption.id, pid:$scope.current_caption.parent_id};
				
				panel.post(location.href, data, function(response){
					
					if(response.Request == 'OK'){
						
						var i = $scope.photos.indexOf($scope.current_caption);
			
						$scope.photos.splice(i, 1);
						
						$("#delete-photo").prop('disabled', true);
			
						panel.alert("Photo deleted!");
					}
				});
			});
		}
		
		$scope.save_captions = function(){
			
			var photos = {q:'captions', photos:$.parseJSON(angular.toJson($scope.photos))};
			
			panel.post(location.href, photos, function(response){
				
				if(response.Request == 'OK'){
					
					location.href = '/upload-business-photos/'+$scope.info.biz.about.id+'?page=captions-thanks-you';
				}else{
					location.href = '/upload-business-photos/'+$scope.info.biz.about.id+'';
				}
			});
		}
	}
	
	$scope.bs = {
		
		prompt_review_type: function(id, name){
			
			$("#review-type-prompt").show();
			
			$scope.textReviewLink = '/review/write?stage=2&type=text&id='+id+'&name='+name;
			
			$scope.videoReviewLink = '/review/write?stage=2&type=video&id='+id+'&name='+name;
		},
		
		pop_business_details: function(){
			
			$("#rating-breakdown").show();
		},
		
		pop_review_details: function(review){
			
			$scope.current_review_q = review.answers;
			
			$scope.current_review_rating = 0;
			
			for(var i in $scope.current_review_q){
				
				$scope.current_review_rating = $scope.current_review_rating + parseInt($scope.current_review_q[i].answer);
			}
			
			$scope.current_review_rating = ($scope.current_review_rating/$scope.current_review_q.length)*10;
			
			$("#review-rating-breakdown").show();
		}
	}
}]);


ascoreControllers.controller('ReviewController', ['$scope','panel','FileUploader','$window', function($scope, panel, FileUploader, $window){
	
	$scope.url_string = function(str){
	
		return str.replace('&', "and").replace(/\s/g, "-").toLowerCase();
	}
	
	$scope.get_province = function(){
		
		return $scope.review.location.province_id;
	}
	
	$scope.doReviewOfType = function(rType){
		
		location.href = location.href.replace('stage=1', "stage=2")+'&type='+rType;
	}
	
	$scope.autosave = function(){
		
		var data = $.extend({}, $scope.review);
		
		delete data.cached; delete data.questions;
		
		panel.post('/review/cache', data, function(response){});
	}
	
	$scope.init_write_review = function(isPartial){
		
		var data = panel.data();
		
		for(var i in data){$scope[i] = data[i];}
		
		if(isPartial){return;}
		
		var isVideo = (typeof $scope.isVideo == 'undefined') ? false : true;
		
		var props = {
			
				autoUpload:true,
				
				url:'/review/cache?id='+$scope.review.business_id,
				
				queueLimit:10,
				
				removeAfterUpload :true
			};
			
		if(isVideo){
			
			var accessToken = $scope.accessToken.access_token;
			
			var params = 'client_id='+$scope.clientId+'&access_token='+accessToken+'&part=snippet&mine=true';
			
			props.url = 'https://www.googleapis.com/upload/youtube/v3/videos?'+params;
		}
		
		$scope.uploader = new FileUploader(props);
		
		if(!isVideo){
			
			$scope.uploader.onBeforeUploadItem = function(item){}
			
			$scope.uploader.onSuccessItem = function(item, response, status, headers){
				
				if(response.Request == 'OK'){
					
					$scope.review.cached.push(response.image);
				}
			}
		}
		else{
			
			$scope.uploader.onBeforeUploadItem = function(item){
				
				$("#loader-image").attr('src', panel.__staticserv()+'/public/images/loading-spinner.gif');
				
				$("#loader-text").html('Uploading...');
			}
			
			$scope.uploader.onErrorItem = function(item, response, status, headers){
				
				$scope.errorUploadingVideo = true;
				
				$("#uploading-review").hide();
				
				try{
					
					console.log(response.error.message);
				}
				finally{
					
					panel.alert("An error occured while uploading the video. Please try again later.");
				}
				
			};
					
			$scope.uploader.onCompleteItem = function(item, response, status, headers){
				
				$scope.videoUploadSuccess = true;
				
				$scope.review.video = response;
				
				$scope.autosave();
			};
		}
		
		$scope.click_uploader = function(elem){
			
			if(elem){
				
				$(elem).click();
			}
			else{
				
				$("#uploader").click();
			}
		}
		
		$(document).ready(function(){
			
			
			
			$("#date_visited").datepicker({
			
				dateFormat: 'yy-mm-dd',
				maxDate: new Date,
				
				
				onSelect:function(){$scope.review.visited = $(this).val(); $scope.autosave();}
			});
			
			if(typeof $scope.review.visited != 'undefined'){
				
				var queryDate = $scope.review.visited,
				
					dateParts = queryDate.match(/(\d+)/g),
					
					realDate = new Date(dateParts[0], dateParts[1] - 1, dateParts[2]);
				
				$("#date_visited").datepicker('setDate', realDate);
			}
			
			if(typeof $scope.review.video != 'undefined' && $scope.review.video.id){
				
				$("#youtube-video").attr('src', 'https://www.youtube.com/embed/'+$scope.review.video.id);
			}
		});
	}
	
	$scope.init_youtube = function(){
		
		$scope.sendYoutubeReview = function(){
			
			 var file = $scope.uploader.queue[0].file;
			
			$scope.youtube.client.setParams({tags:['Verascore Review'], categoryId:22});
			
			panel.fetch('/house/alive', function(response){
			
				if(response.Request == 'OK'){
					
					$scope.youtube.client.uploadFile(file, $scope.review.title, $scope.review.summary);
				}
				else if(panel.ERROR.login_required(response)){
							
					panel.inpage_login(false, function(){console.log('user logged in!!!');});
				}
			});
		}
		
		$scope.youtube = {
			
			video: function(){},
			
			auth: {
				
				BROWSER_KEY: $scope.browserKey,
				
				SCOPES: ['https://www.googleapis.com/auth/youtube']
			},
			
			login: function(gapi){
				
				gapi.auth.authorize({
					
					client_id: $scope.clientId,
					
					scope: $scope.youtube.auth.SCOPES,
					
					immediate: true
				  },
				  
				  function(result){
					
					gapi.client.load('youtube', 'v3', function(){
							
						$scope.youtube.client = new $scope.youtube.VideoUploader(gapi);
					
						$scope.youtube.client.ready($scope.accessToken.access_token);
					  });
				  });
			},
			
			VideoUploader: function(gapi){
				
				this.gapi = gapi;
				
				this.tags = [];

				this.categoryId = 22;

				this.videoId = 0;

				this.uploadStartTime = 0;
			}
		}
		
		var extensions = {
			
			ready: function(accessToken ){
				
				this.accessToken = accessToken;
				
				this.authenticated = true;
				
				this.gapi.client.request({
					
					path: '/youtube/v3/channels',
					
					params: {
						
						client_id: $scope.clientId,
						
						access_token:this.accessToken,
						
						mine:true,
						
						part: 'snippet'
					},
					
					callback: function(response){
							
						if (response.error){
							
							console.log(response.error.message);
						}
						else{
							
							console.log(response.items);
						}
					}.bind(this)
				});
			},
			
			uploadFile: function(file, title, description){
				
				var metadata = {
					
					snippet:{
						
						title: title,
						
						description: description,
						
						tags: this.tags,
						
						categoryId: this.categoryId
					},
					
					status:{
						privacyStatus: 'public'
					}
				};
				
				$scope.uploader.formData = {
					
					metadata: metadata, 
					
					client_id: $scope.clientId,
					
					access_token:this.accessToken.access_token, 
					
					params:{part: Object.keys(metadata).join(',')}
				};
				
				this.uploadStartTime = Date.now();
				
				console.log(this.uploadStartTime);
				
				$scope.uploader.uploadAll ();
			},
			
			setVideoId: function(id){
				
				this.videoId = id;
				
			}.bind(this),
			
			pollForVideoStatus: function(callback){
				
				if(typeof callback != 'undefined'){
					
					this.uploadStatusCallback = callback;
				}
				
				this.gapi.client.request({
					
					path: '/youtube/v3/videos',
					
					params: {
						
						part: 'status,player',
						
						id: this.videoId,
						
						client_id: $scope.clientId,
					
						access_token:this.accessToken, 
					},
					
					callback: function(response){
						panel.alert(response);
						
						var STATUS_POLLING_INTERVAL_MILLIS = 60 * 1000;
						
						if (response.error){
							
							console.log(response.error.message);
							
							setTimeout(this.pollForVideoStatus.bind(this), STATUS_POLLING_INTERVAL_MILLIS);
						}
						else{
							
							var uploadStatus = response.items[0].status.uploadStatus;
							
							switch (uploadStatus){
								
								case 'uploaded':
								
									console.log(uploadStatus);
									
									setTimeout(this.pollForVideoStatus.bind(this), STATUS_POLLING_INTERVAL_MILLIS);
									
								break;
								
								case 'processed':
								
									(this.uploadStatusCallback)();
									
								break;
								
								default:
								
									console.log('Transcoding failed.');
									
								break;
							}
						}
					}.bind(this)
				});
			},
			
			setParams: function(params){for(var i in params){ this[i] = params[i];}}
		}
		
		for(var i in extensions){$scope.youtube.VideoUploader.prototype[i] = extensions[i];}
		
		$window.initYoutube = function(){
			
			if(typeof $window.gapi != 'undefined'){
				
				$scope.youtube.login($window.gapi);
			}
		}
		
		var s = document.createElement('script'); s.src = "//apis.google.com/js/client:plusone.js?onload=initYoutube"; $('html').append(s);
	}
	
	$scope.search_business = function(){
	
		if($scope.business_search.length > 2){
			
			panel.fetch('/search?t=business&j=1&q='+$scope.business_search, function(response){
				
				$scope.searches = response[0];
			});
		}
	}
	
	$scope.set_business = function(business){$scope.businessSelected = business;}
	
	$scope.next_stage = function(){
	
		if($scope.businessSelected){
			
			if($scope.businessSelected.cat_assigned == 'Unclassified'){
				
				location.href = '/review/write?stage=0&id='+$scope.businessSelected.business_id+'&name='+$scope.businessSelected.name;
			}
			else{
				
				location.href = '/review/write?stage=1&id='+$scope.businessSelected.business_id+'&name='+$scope.businessSelected.name;
			}
		}/*
		else{
			
			panel.alert($scope.review);
		}
		
		if(typeof test != 'undefined'){
		
			alert(JSON.stringify($scope.review));return;
		}
		else{
			
			$cookieStore.put('review', $scope.review);
			
			url = url+'&id='+$scope.review.review.business_id+'&name='+encodeURIComponent($scope.business_search);
			
			location.href = location.pathname+url;
			
			$scope.write_view = '/review/'+url;
			
			$("#loader").hide();
		}*/
	}
	
	$scope.get_background = function(percent){
		
		var background = {
			'background':'-webkit-linear-gradient(left, #09C743 0%, #09C743 ' + percent + '%, #CDCDCD ' + percent + '%)'
		};
		
		return background;
	}
	
	$scope.init_question = function(q){
		
		q.answer=0;
		
		if(typeof $scope.review.answers != 'undefined'){
			
			for(var i in $scope.review.answers){
				
				if(typeof $scope.review.answers[i] != 'undefined' && i == q.id){
					
					q.answer=$scope.review.answers[i];
					
					var percent = (q.answer/10)*100;
					
					$(document).ready(function(){
						
						$('.foo-'+q.id+' .bar').each(function(){$(this).removeClass('bold');}); $('.foo-'+q.id+' .bar-'+q.answer).addClass('bold');
						
						$('#answer-'+q.id).css($scope.get_background(percent));
					});
				}
			}
		}
	}
	
	$scope.set_rating = function(q, $event){
			
		var percent = (q.answer/10)*100;
		
		$('.foo-'+q.id+' .bar').each(function(){$(this).removeClass('bold');}); $('.foo-'+q.id+' .bar-'+q.answer).addClass('bold');
		
		$('#answer-'+q.id).css($scope.get_background(percent));
        
		if(typeof $scope.review.answers == 'undefined'){$scope.review.answers = {};}
	
		$scope.review.answers[q.id] = q.answer;
		
		$scope.autosave();
	}
	
	$scope.set_yesno = function(id, answer){
		
		$scope.review['answers'][id] = answer;
	}
	
	
	$scope.load_view = function(){
		
		alert("loaded");
	}
	
	$scope.set_var = function(source,t,k,v){
	
		for(var i in source){
			
			if(source[i]['id'] == v){
				
				$scope.review[t][k] = source[i]['name'];
				
				break;
			}
		}
		
	}
	
	$scope.presave = function(){
	
		$scope.review.business.name = $scope.business_search;
		
		panel.post('/review/presave',$scope.review, function(response){
		
			if(response.Request == 'OK'){
				
				$scope.review.review.business_id = response.business_id;
				
				$scope.destroy();
				
				$scope.view('?stage=3');
			}
		});
	}
	
	$scope.send = function(){
		
		$scope.error = '';
		
		var error = false;
		$scope.error_title='';
		$scope.error_summarize='';
		$scope.error_visited='';
		$('[required]').removeClass('error-field');
		
		if(!$scope.review.title || $scope.review.title.trim().length == 0){
			
			error = true;
			$scope.error_title = 'Review title field is required';
		}
		
		if(!$scope.review.summary || $scope.review.summary.trim().length == 0){
			error = true;
			$scope.error_summarize = 'Summarize field is required';
		}
		
		if(!$scope.review.visited || $scope.review.visited.trim().length == 0){
			
			error = true;
			$scope.error_visited = 'Review visit field is required';
		}
		
		if(error){$scope.error = "*The fields marked in red are required"; return;}
		
		panel.post('/review/save', {id:$scope.review.business_id}, function(response){
			
			if(response.Request == 'OK'){
				
				location.href = response.redirect;
			}
			else if(panel.ERROR.login_required(response)){
					
				panel.inpage_login(false, function(){$scope.send();});
			}
		});
	}
	
	$scope.destroy = function(){
	
		panel.confirm("Are you sure you want to cancel? Any changes you didn't save will be lost!", function(){
			
			panel.post('/review/destroy', {id:$scope.review.business_id}, function(response){
                if (response.Request == 'OK') {
                    $scope.review.title = '';
                    $scope.review.summary = '';
                    $scope.review.visited = '';
                    $scope.review.cached = [];
                }
			});
		});
	}
	
	$scope.read_init = function(){
			
		$scope.paragraph = function(str){
		
			return str.replace(/(?:\r\n|\r|\n)/g, '<br>');
		}
	
		if(typeof $scope.address == 'undefined'){
			
			$scope.address = '';
			
			var address_sections = ['street_line_1','street_line_2','postal_code','city','state','country'];
			
			for(var i in address_sections){
				
				var section = $scope.read.business.location[address_sections[i]];
				
				if( section != null){
					
					$scope.address = $scope.address + section+', ';
				}
			}
			
			$scope.address = $scope.address.substring(0, $scope.address.length - 2);
		}
	
		$scope.get_address = function(){
			
			return $scope.address;
		}
	}
}]);




ascoreControllers.controller('BizRegisterController', ['$scope','panel', function($scope, panel){

	$scope.init = function(){
		
		var data = panel.data();
		
		for(var a in data){
			
			$scope[a] = data[a];
		}
		
		$scope.search = {};
		
		$scope.reg = {location:{country:$scope.active_country}};
		
		$scope.reg.cat_id = 0;
	}
	
	$scope.load_provinces = function(){
            
            panel.fetch('/location/provinces/?country_code='+$scope.reg.location.country,function(response){
                $scope.provinces = response;
                $scope.reg.location.state_id = '';
            }); //test
		/*to be implemented once site goes global - all defaults to Canada for now*/
	}
	
	$scope.set_search = function(k, v){
		
		$scope.search[k] = v;
		
		$scope.search.city = {};
	}
	
	$scope.get_search = function(k){
		
		return $scope.search[k];
	}
	
	$scope.search_business = function(){
	
		panel.fetch('/search?t=business&j=1&q='+$scope.business_search, function(response){
			
			$scope.searches = response;
				
		});
	}
	
	$scope.set_main_cat = function(){
	
		$scope.reg.business.sub_cat_id = 0;
		
		for(i in $scope.cats){
			
			if($scope.cats[i]['id'] == $scope.reg.business.cat_id){
			
				$scope.main_cat = $scope.cats[i];break;
			
			}
		}
	}
	
	$scope.set_sub_cat = function(){
	
		$scope.reg.business.sub_cat_id = $scope.sub_cat;
	}
	
	$scope.set_error = function(_class, error){
		
		var _cls = "."+_class;
		
		$(_cls).html(error);
		
		if(error == ''){
			
			$(_cls).siblings().css({'border-color':'#cccccc'});
		}
		else{
			
			$(_cls).siblings().css({'border-color':'red'});
		}
	}
	
	$scope.register_biz = function(){
		
		if($scope.search && $scope.search.city){$scope.reg.location.city = $scope.search.city.name;}
		
		var errors = false;
		
		$scope.set_error('error', '');
		
		var required = {
			
			business:['name','cat_id'],
			
			location:['city','postal_code','country','street_line_1','state_id'],
			
			profile:['telephone','email']
		};
		
		for(var i in required){
			
			var fields = required[i];
			
			if(typeof $scope.reg[i] != 'undefined'){
				
				var check = $scope.reg[i];
				
				for(var j in fields){
					
					var field = fields[j];
					
					if(typeof check[field] == 'undefined' || check[field] == ''){
						
						errors = true;
						
						var _class = i+'-'+field;
						
						$scope.set_error(_class, 'Please enter a value for this field');
					}
				}
			}
			else{
				
				errors = true;
				for (var j in fields) {
                    var _class = i+'-'+fields[j];
				
                    $scope.set_error(_class, 'Please enter a value for this field');
                }
			}
		}
		
		if(errors) return;
        
        $scope.checkLocation(function () {

            panel.continueIfAccess(function () {

                panel.post('/business/register', $scope.reg, function (response) {

                    if (response.Request == 'OK') {

                        window.location = response.redirect;
                    } else {

                        panel.alert(response.Error);
                    }
                }, false, true);
            });
        });
	}
    
        $scope.checkLocation = function (callback) {
            console.log("cheking location...");
            panel.post('/business/location', $scope.reg.location, function (response) {
                if (response.location == 'invalid') {
                    panel.alert("Provided location is invalid. Please enter valid location");
                    return false;
                } else if (response.location == 'valid') {
                    if (callback) {
                        (callback)();
                    }
                }
            }, false, true);
        };
	
}]);



ascoreControllers.controller('BizManagementController', ['$scope','panel', function($scope, panel){

	
	/*panel.alert(JSON.stringify($scope.hours));*/

	$scope.init = function(){
		
		$scope.hours = [];
		
		for(i = 0 ; i < 13 ; i++){$scope.hours[i] = (i<10) ? '0'+i : i.toString();}
		
		$scope.minutes = [];
		
		for(i = 0 ; i < 60 ; i++){$scope.minutes[i] = (i<10) ? '0'+i : i.toString();}
		
		var weekdays = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];
		
		$scope.weekdays = [];
		
		for( var i = 0 ; i < weekdays.length ; i++ ){
			
			var day = {'id': i.toString(), 'day':weekdays[i]};
			
			$scope.weekdays[i] = day;
		}
		
		var data = panel.data();
		
		if(data){
		
			for(var a in data){
				
				$scope[a] = data[a];
			}
		}
		
		$scope.set_main_cat = function(id, reset){
			
			if(!reset) {
                $scope.biz.about.subcats = [];
            }
			
			for(var i in $scope.cats){
				
				if($scope.cats[i].id == id){
					
					$scope.main_cat = $scope.cats[i]; $scope.the_main_subcat = 0;
                    for (j in $scope.cats[i].subcats) {
                        $scope.cats[i].subcats[j].selected = 0;
                    }
                    break;
				}
			}
		}
		
		if(typeof $scope.the_main_cat != 'undefined'){
			
			$scope.set_main_cat($scope.the_main_cat, 1);
			
			$scope.the_main_subcat = 0;
			
			if($scope.biz.about.category_id != $scope.the_main_cat){
				
				$scope.the_main_subcat = $scope.biz.about.category_id;
			}
		}
		
		if($scope.biz.profile.length == 0){
			
			$scope.biz.profile = {bs_register_id:$scope.biz.about.id};
		}
		
		$scope.set_new_contact = function(){
		
			$scope.new_contact = {bs_register_id:$scope.biz.profile.bs_register_id,phone:'',type:'landline',who:''};
		}
		
		$scope.set_new_contact();
		
		$scope.update_contact = function(data){
			
			if(typeof data == 'undefined'){data = $scope.new_contact};
			
			if(data.phone.length != 10 || !/^\+?(0|[1-9]\d*)$/.test(data.phone)){panel.alert("Please enter a valid 10 digit phone number."); return;};
			
			panel.post('/business/update', {q:'update_contact', info:data}, function(response){
				
				if(response.Request == 'OK'){
					
					$scope.biz.contact.push(response.contact);
					
					$scope.set_new_contact();
				}
			});
		}
		
		$scope.remove_contact = function(id){
			
			panel.confirm("Are you sure you want to delete this contact?", function(){
				
				panel.post('/business/update', {q:'remove_contact', id:id}, function(response){
					
					for(i in $scope.biz.contact){
						
						if($scope.biz.contact[i]['id'] == id){
							
							$scope.biz.contact.splice(i, 1); break;
						}
					}
				});
			});
		}
	}
	
	$scope.set_subcat_status = function(i, index){
		
		var j = $scope.biz.about.subcats.indexOf(i.id);
		
		(j > -1) ? i.selected = 1 : i.selected = 0;
	}
	
	$scope.update_subcats = function(i, index){
		
		var j = $scope.biz.about.subcats.indexOf(i.id);
		
		if(j > -1){
			
			$scope.biz.about.subcats.splice(j, 1);
			
			i.selected = 0;
		}
		else{
			
			$scope.biz.about.subcats.push(i.id);
			
			i.selected = 1;
		}
	}
	
	$scope.convert_features_to_object = function(){
		
		if( typeof $scope.biz.feature_options.length != 'undefined'){
			
			var items = {};
			
			for(var i in $scope.biz.feature_options){
				
				items[$scope.biz.feature_options[i]['id']] = $scope.biz.feature_options[i];
			}
			
			$scope.biz.feature_options = items;
		}
	}
	
	$scope.set_feature_option = function(option, index){
		
		var feature = option.id;
		
		var _cls = $('.feature-'+feature), toggle = 'selected-checkbox-1';
		
		if(_cls.hasClass(toggle)){_cls.removeClass(toggle);}else{_cls.addClass(toggle);}
		
		$scope.convert_features_to_object();
		
		if(typeof $scope.biz.feature_options[option.id] == 'undefined'){
			
			$scope.biz.feature_options[option.id] = option;
			
			var opt = option.option.toLowerCase();
			
			if(opt == 'no' || opt == 'yes'){
				
				var feats = $scope.features[index].options, is_bool = false;
				
				for(var i in feats){
					
					var item = feats[i];
					
					var opt2 = item.option.toLowerCase();
					
					if(item.id != feature && (opt2 == 'no' || opt2 == 'yes')){
						
						is_bool = true; break;
					}
				}
				
				if(is_bool){
					
					_cls = $('.feature-'+item.id), toggle = 'selected-checkbox-1';
			
					if(_cls.hasClass(toggle)){
						
						_cls.removeClass(toggle);
						
						delete $scope.biz.feature_options[item.id];
					}
				}
			}
		}
		else{
			
			delete $scope.biz.feature_options[option.id];
		}
	}
	
	$scope.profile_update_info = function(){
		
		$("#editor").show();
		$("#profile").hide();
		$("#up-hide").show();
		$("#up-show").hide();
	}
	
	$scope.get_weekday = function(day){return $scope.weekdays[day]['day'];}
	
	$scope.get_provinces = function(country_id){
		
		panel.fetch('/house/data?query=provinces&id='+country_id, function(response){
			
			if(response.Request == 'OK'){
				
				$scope.biz.location.state_id = 0;
				
				$scope.provinces = response.provinces;
			}
		}, true);
	}
	
	$scope.set_error = function(_class, error){
		
		var _cls = "."+_class;
		
		$(_cls).html(error);
		
		if(error == ''){
			
			$(_cls).siblings().css({'border-color':'#09C645'});
		}
		else{
			
			$(_cls).siblings().css({'border-color':'red'});
		}
	}
	
	$scope.validated = function(data){
		
		var errors = false;
		
		$scope.set_error('error', '');
		
		var required = {
			
			about:['name','category_id'],
			
			location:['city','postal_code','country_id','street_line_1','state_id'],
			
			profile:['telephone','email']
		};
		
		for(var i in required){
			
			var fields = required[i];
			
			var check = data[i];
			
			for(var j in fields){
				
				var field = fields[j];
				
				if(typeof check[field] == 'undefined'  || check[field] == null || check[field].trim() == '' && check[field] != 0){
					
					errors = true;
					
					var _class = i+'-'+field;
					
					$scope.set_error(_class, 'Please enter a value for this field');
				}
			}
		}
		
		return !errors;
	}
    
    $scope.checkLocation = function (callback) {
 
            console.log("cheking location...");
 
            panel.post('/business/location', $scope.biz.location, function (response) {
                console.log(response);
                if (response.location == 'invalid') {
 
                    panel.alert("Provided location is invalid. Please enter valid location");
 
                    return false;
 
                } else if (response.location == 'valid') {
 
                    if (callback) {
 
                        (callback)();
 
                    }
 
                }
 
            }, false, true);
 
        };
 
	
	$scope.profile_save_changes = function(features){
	$scope.checkLocation(function() {
        
		if(typeof features == 'undefined'){
			
		//	$scope.biz.about.category_id = ($scope.the_main_subcat == 0) ? $scope.the_main_cat : $scope.the_main_subcat;
		
			var data = $.extend({}, $scope.biz);
		
			if($scope.validated(data) == false){
				
				return;
			}
			
			 data.profile.hours = angular.toJson(data.profile.hours);
			 
			 data.schedule  = angular.toJson(data.schedule);
			 
			 if($scope.biz.about.subcats.length > 3 && $scope.biz.about.is_sponsored == 0){
			
				panel.alert("You are not allowed to enter more than 3 sub categories. Please adjust your categories.");
				
				return;
			}
			
			if($scope.biz.location.neighborhood.split(',').length > 3){
				
				panel.alert("You are not allowed to enter more than 3 Neighborboods. Please adjust to hoods to the required amount.");
				
				return;
			}
		}
		else{
			
			$scope.convert_features_to_object();
			
			var data = $.extend({}, $scope.biz);
			
			data.feature_options = angular.toJson(data.feature_options);
		}
		
		panel.post('/business/update', data, function(response){
			
			if(response.Request == 'OK'){
				
				for(var j in response.data){
					
					$scope[j] = response.data[j];
				}
				
				panel.alert("Your changes have been saved.");
			/*	$scope.biz.profile = response.profile; */
			} else if (response.Error !== '') {
                panel.alert(response.Error);
            }
		}, true, true);
    });
	}
	
	$scope.updatePrimaryPhoto = function(){$("#fileupload").click();}
	
	$scope.uploadPrimaryPhoto = function(data){
		
		var photo = new FormData();
		
		for(var i in data){photo.append('primary', data[i]);}
		
		photo.append('data', JSON.stringify({id:$scope.biz.about.id}));
		
		panel.upload('/incoming/biz', photo, function(response){
		
			if(response.Request == 'OK'){
				
				$scope.biz.profile.photo = response.primary;
				
				$scope.$apply();
			}
		}, false, true);
	}
	
	$scope.init_manager = function(){
		
		$scope.role_description = '';
		
		$scope.default_role = '2';
		
		$scope.set_role_description = function(role){
			
			for(var i in $scope.roles){
				
				if($scope.roles[i]['id'] == role){
					
					$scope.role_description = $scope.roles[i]['description']; break;
				}
			}
		}
		
		$scope.set_role_description($scope.default_role);
		
		$scope.search_user_admin_email = function(){
			
			$scope.user_search_results = $scope.user_search_results_error = '';
			
			panel.fetch('/house/data?query=manage_user_admin&email='+$scope.search_user_admin, function(response){
				
				if(response.Request == 'OK'){
					
					$scope.user_search_results = response.user;
				}
				else{
					
					$scope.user_search_results_error = 'The email you entered was not found.';
				}
			});
		}
		
		$scope.set_permissions = function(){$("#grant-permissions").show();}
		
		$scope.grant_permissions = function(){
			
			var data = {q:'grant_permissions', permissions:{business_id:$scope.biz.about.id,admin_id:$scope.user_search_results.id,role_id:$scope.default_role,added_by:0}};
			
			panel.post('/business/update', data, function(response){
			
				if(response.Request == 'OK'){
					
					$scope.management = response.management;
					
					$("#grant-permissions").hide();
			
					$scope.user_search_results = $scope.search_user_admin = '';
				}
			});
		}
		
		$scope.set_revoke_permissions = function(u){$scope.begone_from_us = u;$("#revoke-permissions").show();}
		
		$scope.revoke_permissions = function(){
			
			var data = {q:'revoke_permissions', id:$scope.begone_from_us.management_id};
			
			panel.post('/business/update', data, function(response){
			
				if(response.Request == 'OK'){
					
					var x = $scope.management.indexOf($scope.begone_from_us);
					
					$scope.management.splice(x, 1);
					
					$("#revoke-permissions").hide();
				}
			});
		}
		
		$scope.remove_popup = function(item){$(item).hide();}
	}
	
	$scope.init_account = function(){
		
		$scope.cc = {};
		
		$scope.exp_years = [];
		
		$scope.exp_months = [];
		
		var d = new Date();
		
		var n = d.getFullYear();
		
		for(i = n ; i < n+10 ; i++){$scope.exp_years[i-n] = i;}
		
		$scope.exp_months[0] = "01";
		$scope.exp_months[1] = "02";
		$scope.exp_months[2] = "03";
		$scope.exp_months[3] = "04";
		$scope.exp_months[4] = "05";
		$scope.exp_months[5] = "06";
		$scope.exp_months[6] = "07";
		$scope.exp_months[7] = "08";
		$scope.exp_months[8] = "09";
		$scope.exp_months[9] = "10";
		$scope.exp_months[10] = "11";
		$scope.exp_months[11] = "12";
		
		var s = document.createElement('script'); s.src = "https://js.stripe.com/v2/"; $('html').append(s);
	}
	
	$scope.set_subscription_desc = function(package_id){
		
		var subs = $.extend({}, $scope.subscription_packages);
		
		for(var i in subs){
		
			if(subs[i]['id']==package_id){
				
				$scope.subscription_desc = subs[i]['description'];
				
				break;
			}
		}
	}
	
	$scope.create_account = function(){
		
		var card = $scope.cc;
		
		Stripe.setPublishableKey('pk_test_mcylwtHGpusqoJs6HKdg06dT');
		
		Stripe.card.createToken(
		
			card,
			
			function(status, response){
				
				if (response.error){
					
					$scope.create_account_error = response.error.message;
					
				}
				else{
					
					$scope.create_account_error = '';
					
					var data = {package_id:$scope.package_id,business_id:$scope.biz.about.id, stripe: JSON.stringify(response)};
					
					panel.post('/business/account/open', data, function(response){
						
						if(response.Request == 'OK'){
							
							location.reload();
						}
					});
				}
			}
		);
	}
	
	$scope.request_account_setup = function(){
		
		panel.post('/business/account/request_sponsor_access', {business_id:$scope.biz.about.id}, function(response){
						
			if(response.Request == 'OK'){
				
				$scope.sponsor_requested = true;
			}
		}, false, true);
	}
	
	$scope.get_address = function(){
		
		return $scope.biz.location.street_line_1+' '+
				$scope.biz.location.street_line_2+' '+
				$scope.biz.location.postal_code+' '+
				$scope.biz.location.city+' '+
				$scope.biz.location.state+' '+
				$scope.biz.location.country;
	}
}]);



ascoreControllers.controller('FeedbackController', ['$scope','panel', function($scope, panel){

	$scope.init = function(){
		
		var data = panel.data();
		
		for(var i in data){$scope[i] = data[i];}
		
		$scope.answers = {};
		
		$scope.rating_numbers = [1,2,3,4,5];
	}
	
	$scope.recaptchaPass = function(){
	
		return true;
		
		var captcha = grecaptcha.getResponse();
		
		$scope.feedback.captcha = captcha;
		
		return (captcha.length > 0);
	}
	
	$scope.post_feedback = function(){
		
		var has_error = false;
		
		$scope.error = '';
		
		$('.feedback-fullname,.feedback-email,feedback-comment').removeClass('fix_error');
		
		if(!$scope.feedback.comment || $scope.feedback.comment.trim().length==0){
			
			$scope.feedback.comment = '';
			
			$('.feedback-comment').addClass('fix_error');
			
			has_error = true;
		}
		
		var ignore_to_lighten_things_up_for_a_few_days = function(){
			
			if(!$scope.feedback.fullname || $scope.feedback.fullname.trim().length==0){
				
				$scope.feedback.fullname = '';
				
				$('.feedback-fullname').addClass('fix_error');
				
				has_error = true;
			}
			
			if(!$scope.feedback.email || $scope.feedback.email.trim().length==0){
				
				$scope.feedback.email = '';
				
				$('.feedback-email').addClass('fix_error');
				
				has_error = true;
			}
		};
		
		if(has_error){$(window).scrollTop(0);$scope.error = "Please fix the highlighted fields. It's the most important information we require."; return;}
		
		if($scope.recaptchaPass()){
			
			var data = {feedback:$scope.feedback, answers:$scope.answers};
			
			panel.post('/feedback/add', data, function(response){
				
				if(response.Request == 'OK'){
					
					location.href = '/feedback/thankyou?ref='+response.ref;
				}
				else{
					
					$scope.error = response.Error;
					
					$(window).scrollTop(0);
				}
			});
		}
	}
}]);


ascoreControllers.controller('LoginController', ['$scope','panel', function($scope, panel){

	$scope.error = '';
	
	$scope.entry_error = '';
	
	$scope.register = {first_name:'',last_name:''};
	
	$scope.checkItem = function(name, error){

		$scope.error = '';
		
		var data = {};
		
		data[name] = $scope.register[name];
		
		panel.post('/access/check', data, function(response){
			
			if(response.Request == 'Error'){
			
				if(error){
					
					$scope.error = error;
				}
				else{
					
					$scope.entry_error = "The "+name+" you entered is not available.";
				}
			}
			else{
			
				$scope.entry_error = '';
			}
				
		});
	}
	
	$scope.checkPassword = function(){
	
		if($scope.entry_error == ''){
		
			if($scope.register.passwd_1.length < 8){
			
				$scope.error = 'Password too short. Minimum 8 characters';
			}
			else if($scope.register.passwd_1 != $scope.register.passwd_2){
			
				$scope.error = 'Your passwords do not match.';
			}
			else{
			
				$scope.error = '';
			}
		}
	}
	
	$scope.reset_password = function(){
		
		$scope.checkPassword();
		
		if($scope.error == '' && $scope.register.passwd_1.length>=8){
			
			panel.post('/access/reset', {password:$scope.register.passwd_1,token:$scope.passwd_reset_token}, function(response){
				
				if(response.Request == 'OK'){
					
					location.href = '/access';
				}
			}, false, true);
		}
	}
	
	$scope.recaptchaPass = function(){
	
		var captcha = grecaptcha.getResponse();
		
		$scope.register.captcha = captcha;
		
		return (captcha.length > 0);
	}
	
	$scope.next = function(inline){

		if($scope.recaptchaPass()){
		
			panel.post('/access/register', $scope.register, function(response){
			
				if(response.Request == 'OK'){
					
					if(typeof inline == 'undefined'){
				
						location.href = response.redirect;
					}
					else{
					
						panel.inpage_login(true);
						
						if(panel.inpage_callback.length == 1){
							
							(panel.inpage_callback[0])();
						}
					}
				}
					
			}, false, true);
		}
	}
	
	
	$scope.signin = function(inline, typing){
		
		var keyPress = false;
		
		if(inline && inline.keyCode){
			
			keyPress = true;
			
			if(inline.keyCode != 13){
				
				return;
			}
		}
		
		panel.post('/access/login', $scope.signin, function(response){
			
			if(response.Request == 'OK'){
			
				if(typeof inline == 'undefined' || keyPress){
					
					var url = atob(panel.getParam('red'));
					
					if(url.length > 0){
						
						location.href = url;
					}
					else{
						
						location.href = response.redirect;
					}
				}
				else{
					
					panel.inpage_login(true);
					
					if(panel.inpage_callback.length == 1){
						
						(panel.inpage_callback[0])();
					}
				}
			}
			else{
			
				$scope.login_error = "Login error! Check username or password.";
			}
				
		}, true);
	}
	
	$scope.settings = {
		
		check: function(){$scope.checkItem('username', 'Profile url not available.');},
		
		update: function(){
			
			$scope.error_pw = '';
			
			if($scope.register.username || $scope.register.passwd_1){
				
				if(!$scope.register.passwd || $scope.register.passwd.trim().length == 0){
					
					$scope.error_pw = 'Please enter your old password.';
				}
				if (/\s/.test($scope.register.passwd)) {
    			$scope.error_pw ='spaces are not allowed';	
				return false;
				}
				
				if($scope.error == '' && $scope.error_pw == ''){
					
					panel.post('/access/update', $scope.register, function(response){
						
						if(response.Request == 'OK'){
							
							panel.alert("Thank you! Your changes have been saved!");
							
							$scope.register = {};
							
							if(response.reload_url){
								
								location.href = response.reload_url;
							}
						}
						else{
							
							$scope.error_pw = response.Error;
						}
					});
				}
			}else{
			$scope.error_pw = 'Please fill password fields';	
			}
		}
		
		
	}
	
	$scope.send_reset_link = function(){
		
		$scope.reset_error = $scope.reset_successful = '';
		
		panel.post('/access/reset', {email:$scope.reg_email}, function(response){
			
			if(response.Request == 'OK'){
				
				$scope.reset_successful = "You request was processed successfully. You will receive an email reset link shortly.";
			}
			else{
				
				$scope.reset_error = "Your email did not match our records.";
			}
		});
	}
	
	$scope.recaptureJS = function(){
		
		var s = document.createElement("script");
		
		s.src = "https://www.google.com/recaptcha/api.js"; $("html").append(s);
	}
	
	$scope.mobile = {
		
		display_login: function(show){
			
			show ? $('.access-signin').show() && $('.access-signup').hide() : $('.access-signin').hide() && $('.access-signup').show();
		}
	}
}]);



ascoreControllers.controller('CommunityController', ['$scope','panel','$compile', function($scope, panel, $compile){
	    
    $scope.isEmptyAnswer = false;  

	$scope.init = function(bind){
		
		if(bind){
			
			var data = panel.data(bind);
		}
		else{
			var data = panel.data();
		}
		
		for(var i in data){$scope[i] = data[i];}
		
		$scope.search_query = '';
		
		$scope.pagelist = [];
        
        var max_page = Math.ceil($scope.total / $scope.per_page);

        var min;
        var center = Math.floor($scope.pl_size / 2);
        var page = parseInt($scope.page);

        if (page - center < 1 && page + center > max_page) {
            min = 1;
            max = max_page;
        } else if (page - center < 1) {
            min = 1;
            max = min + $scope.pl_size - 1;
        } else if (page + center > max_page) {
            max = max_page;
            min = max_page - $scope.pl_size + 1;
        } else {
            min = page - center;
            max = min + $scope.pl_size - 1;
        }
            
		for(var i = min ; i <= max ; i++){$scope.pagelist.push(i);}
		
		$scope.showing_from = 1;
			
		$scope.showing_to = $scope.per_page;
		
		if($scope.page > 1){
			
			$scope.showing_from = ($scope.page*$scope.per_page)-$scope.per_page+1;
			
			$scope.showing_to = $scope.showing_from+$scope.questions.length-1;
		}
		
		$(document).ready(function(){
			
			if($("#init-activity")){
				
				$('#ajax-view-display').css({'z-index':2});
				
				$('.profile-view.other').show();
				
				$('.profile-view.basic').hide();
				
				$scope.interact.register_click_event();
				
				if($scope.page == 'questions'){
						
					$("#mobile-title").text('My Questions ('+$scope.size+')');
				}
				else{
					
					$("#mobile-title").text('Questions by friends ('+$scope.size+')');
				}
			}
		});
	}
	
	$scope.interact = {
		
		register_click_event: function(){
			
			$('#rewinder').unbind("click");
			
			$('#rewinder').click(function(){$scope.interact.rewind();});
		},
		
		ajax: function(page){
			
			$scope.interact.register_click_event();			
			
			var url = '/user/'+$scope.user.username+'/'+page+'?json=true&ajax=true';
			
			panel.fetch(url, function(response){
				
				panel.loading(false);
				
				$('body, html').animate({scrollTop: 0}, 500);
				
				var content = $(response);
				
				$compile(content)($scope);
				
				$('#ajax-viewer').html(content);
				
				$('#ajax-view-display').css({'z-index':2});
				
				$('.profile-view.other').show();
				
				$('.profile-view.basic').hide();
				
				$scope.init('#init-activity');
				
				if(page == 'questions'){
					
					$scope.mobile_title = 'My Questions ('+$scope.size+')';
				}
				else{
					
					$scope.mobile_title = 'Questions by friends ('+$scope.size+')';
				}
				
				$(document).ready(function(){$("#mobile-title").html($scope.mobile_title);});
				
				$scope.user_page_loader();
				
			}, true, true);
		},
		
		rewind: function(){
			
			$('#ajax-viewer').html('');
				
			$('#ajax-view-display').css({'z-index':-100});
			
			$('.profile-view.other').hide();
			
			$('.profile-view.basic').show();
			
			window.history.back();
		},
		
		visitor_page: function(username, first_name, page){
			
			var url = '/user/'+username+'/'+page+'?json=true&ajax=true';
			
			panel.fetch(url, function(response){
				
				panel.loading(false);
				
				$("#slide_title").html('Questions by '+first_name);
				
				$('body, html').animate({scrollTop: 0}, 100);
				
				var content = $(response);
				
				$compile(content)($scope);
				
				$('.my-slide-content').html(content);
				
				var data = panel.data('#init-activity'); for(var i in data){$scope[i] = data[i];} $scope.init_ckeditor();
				
				$('.my-slide').show();
				
				$('header').hide();
				
				$('.my-page').hide();
				
			}, true, true);
		}
	}
	
	$scope.user_page_loader = function(){
		
		$(document).ready(function(){
			
			$(window).scroll(function(){
				
				var loadHeight = $(document).height() - $(window).height();
				
				if($(window).scrollTop() == loadHeight){
					
					if(!$scope.page_no){$scope.page_no = 1;$scope.fetch_more_activity = true;}
					
					if($scope.fetch_more_activity){
						
						var params = panel.getParams();
								
						var url = '/user/'+$scope.user.username+'/questions?json=true&page_no='+$scope.page_no;
						
						if(params){for(var i in params){url += '&'+i+'='+params[i];}}
						
						panel.fetch(url, function(response){
							
							if(typeof response.fetch == 'undefined'){
								
								for(var i in response){
									
									$scope.questions.push(response[i]);
								}
								
								if(!$('.back_to_top').length){$('.middle').not('.top').append('<a class="back_to_top blue" style="float:right;" href="#top">Back to top</a>');}
								
								$scope.page_no++;
							}
							else{
								
								$scope.fetch_more_activity = false;
							}
						});
					}
				}
			});
		});
	}
	$scope.set_selected_page = function(page){if($scope.page == page){return 'selected';}}
	
	$scope.index = function(i){if($scope.page > 1){return ($scope.per_page*($scope.page-1))+i+1;}return i+1;}
	
	$scope.init_ckeditor = function(){
		
		var s = document.createElement("script");
		
		s.src = "https://cdn.ckeditor.com/4.5.7/standard/ckeditor.js"; $("html").append(s);
	}
	
	$scope.searchCommunity = function($event){
		
		if((!$event || $event.keyCode == 13) && $scope.search_query.trim().length > 0){
			
			location.href = '/community/search/query/'+$scope.search_query;
		}
	}
	
	$scope.reported_question = function(response){
		
		if($scope.questions){
			
			for(var i in $scope.questions){
				
				if($scope.questions[i]['id'] == response.content_id){
					
					$scope.questions[i]['reported'] = true;
					
					$scope.questions[i]['flags'].size = parseInt($scope.questions[i]['flags'].size)+1;
					
					break;
				}
			}
		}
		else{
			
			$scope.question['reported'] = true;
					
			$scope.question['flags'].size = parseInt($scope.question['flags'].size)+1;
		}
	}
	
	$scope.reported_answer = function(response){
		
		for(var i in $scope.question.answers){
				
			if($scope.question.answers[i]['id'] == response.content_id){
				
				$scope.question.answers[i]['reported'] = 1;
				
				$scope.question.answers[i]['flags'].size = parseInt($scope.question.answers[i]['flags'].size)+1;
				
				break;
			}
		}
	}
	
	$scope.ask = {
		
		edit_words: function(index, word, context){
			
			$scope.edit_word_index = index; $scope.editing_word = word;
			
			$('.save-input').hide();
			
			$("#edit-"+context).show();$("#edit-"+context).focus();
		},
		
		hide_input: function(){$('.save-input').hide();},
		
		save_word: function($event, context, word){
			
			if($event.keyCode == 13){
				
				if(word.trim().length > 0){
					
					if($scope.edit_word_index < 0){
						
						$scope.question[context].push(word.trim());
					}
					else{
						
						$scope.question[context].splice($scope.edit_word_index, 1, word.trim());
					}
				}
				else{
					
					if($scope.edit_word_index > 0){
						
						$scope.question[context].splice($scope.edit_word_index, 1);
					}
				}
				
                        $scope.edit_word_index = -1; $scope.editing_word  = '';
                        $("#edit-"+context).val('');
			}
		},
		
		edit_question: function(hide){
			
			if(!hide){$("#edit-question").show();$("#edit-question").focus();}
			else{$("#edit-question").hide();}
		},
		
		set_qtype: function(type){ $scope.question.question_type = type;},
		
		
		evaluate_question: function($event){
			
			var str = $scope.new_question;
			
			var prompt = $(".question-mark-bubble");
			
			if($event && $event.keyCode != 13){
				
				!prompt.hasClass('hide') && prompt.addClass('hide');
				
				return;
			}
			
			if(str.charAt(str.trim().length-1) != '?'){
				
				prompt.removeClass('hide');
				
				return;
			}
			
			location.href = '/community/ask?question='+str;
		},

		ask_question: function(){
			
			$scope.error = {};
			
			if($scope.question.title.trim().length == 0){
				
				$scope.error.title = 'Please enter a brief title';
			}
			
			if($scope.question.tags.length == 0){
				
				$scope.error.tags = 'Please enter at least one keyword';
			}
			
			if($scope.question.categories.length == 0){
				
				$scope.error.category = 'Please enter at least one category';
			}
			
			if(panel.sizeOf($scope.error) > 0){return;}
			
			var callback = function(){
						
				panel.post('/community/ask', $scope.question, function(response){
					
					if(response.Request == 'OK'){ location.href = response.redirect; }
				});
			};
			
			panel.fetch('/house/alive', function(response){
				
				if(response.Request == 'OK'){
					
					(callback)();
				}
				else if(panel.ERROR.login_required(response)){
				 		
					panel.inpage_login(false, function(){
						
						(callback)();
					});
				}
			}, true);
		}
	}
	
	$scope.ckEditorConfig = {
		uiColor: '#FFFFFF',
		height:'125px',
		toolbar:[
			{name:'basicstypes', items:['Bold', 'Italic']},
			{name:'paragraph', items:['NumberedList', 'BulletedList']}
		]
	}
	
	$scope.answer = {
		
		load_editor: function(q){
    
     panel.fetch('/house/alive', function(response){
				
				if(response.Request == 'OK'){
					var pad = $("#pad-id-"+q.id);
			
		     	pad.removeClass('hide');
			
			    var editor = "editor-"+q.id;
			
			    CKEDITOR.replace(editor, $scope.ckEditorConfig);
					(callback)();
				}
				else if(panel.ERROR.login_required(response)){
							
					panel.inpage_login(false, function(){
						
						(callback)();
					});
				}
			}, true);
			
			
		},
        
        cancel_answer: function (q) {
 
            var editor = "editor-"+ q.id;
 
            CKEDITOR.instances[editor].setData('');  
            $("#pad-id-" + q.id).addClass('hide');  
              
        },  
		
		post_answer: function(q){
			
			var editor = "editor-"+q.id;
			
			var data = CKEDITOR.instances[editor].getData();
			
			var answer = !q.response ? {} : q.response;
			
			answer.question_id = q.id;
            
			answer.answer = data;
            if (answer.answer.length>0) {
                
                $scope.isEmptyAnswer = false;
                panel.post('/community/answer/' + q.id, answer, function (response) {

                    if (response.Request == 'OK') {

                        if ($scope.questions) {

                            var i = $scope.questions.indexOf(q);

                            $scope.questions[i].responses.size = parseInt($scope.questions[i].responses.size) + 1;

                            $scope.questions[i].response.twitter = 0;

                            $scope.questions[i].response.facebook = 0;

                            $("#pad-id-" + q.id).addClass('hide');
                        } else {

                            $scope.question.answers.push(response.answer);

                            $scope.question.responses.size = parseInt($scope.question.responses.size) + 1;

                            $scope.question.response.twitter = 0;

                            $scope.question.response.facebook = 0;

                            $("#pad-id-" + $scope.question.id).addClass('hide');
                        }

                        CKEDITOR.instances[editor].setData('');
                    }
                });

            } else {
                $scope.isEmptyAnswer = true;
            }
        }
	}
	
	$scope.search_community = function(keyCode){
		
		if(keyCode == 13){
			
			location.href= '/search?t=community&q='+$scope.query;
		}
	}
	
	$scope.follow_question = function(question, action){
		
		var id = action == 'follow' ? question.id : question.following.id;
		
		panel.post('/community/action', {id:id, action:action}, function(response){
			
			if(response.Request == 'OK'){
				
				for(i in $scope.questions){
					
					if($scope.questions[i]['id'] == question.id){
						
						$scope.questions[i]['following'] = response.following;
						
						break;
					}
				}
			}
			
		});
	}
	
	$scope.endorse_question = function(question_id, opinion){
		
		panel.post('/community/action', {question_id:question_id, opinion:opinion, action:'endorse'}, function(response){
			
			if(response.Request == 'OK'){
				
				$scope.question.good_discussion.size = parseInt($scope.question.good_discussion.size) + 1;
			}
		});
	}
	
	$scope.approve_answer = function(answer_id, opinion){
		
		panel.post('/community/action', {answer_id:answer_id, opinion:opinion, action:'approve'}, function(response){
			
			if(response.Request == 'OK'){
				
				for(var i in $scope.question.answers){
					
					if($scope.question.answers[i]['id'] == answer_id){
						
						$scope.question.answers[i]['good_answer']['size'] = parseInt($scope.question.answers[i]['good_answer']['size']) + 1;
						
						break;
					}
				}
			}
			
		});
	}
	
	$scope.url_string = function(str){
	
		return panel.url_string(str);
	}
	
	$scope.community_keywords = function(str){
	
		return panel.community_keywords(str);
	}
	
	$scope.get_feed_message = function(){
		
		var feeds = {
			recommended:'You have no recommended questions',
			responses:'There are no responses to any asked questions',
			following:'You are not following any questions',
			asked:'Noone has asked you a question yet'
		}
		
		return feeds[$scope.feed];
	}
	
}]);


ascoreControllers.controller('ProfileController', ['$scope','panel', '$compile', function($scope, panel, $compile){
/*'$route', '$routeParams', '$location'*/
	$scope.path = location.pathname;
	$scope.showSpinner = false;
	$scope.disableLink = false;
	$scope.init = function(){
		
		var data = panel.data();
		
		for(var i in data){
		
			$scope[i] = data[i];
		}
        var currentDate = new Date();
        var brithday = new Date($scope.profile.birthday);

        if (!isNaN(brithday.getFullYear())) {
            $scope.profile.age = currentDate.getFullYear() - brithday.getFullYear();
        }
        
		$(document).ready(function(){
			
			var active_page = '.view-'+$scope.active_page;
			
			$(active_page).addClass('active_page');
			
			$(active_page+' .view-show').hide();
			$(active_page+' .view-hide').show();
			
			$("[datepicker]").datepicker({
			
				dateFormat: 'yy-mm-dd',
				changeMonth: true,
				changeYear: true,
				showButtonPanel: true,
				yearRange: '-100:-12',
				onClose:function(){$scope.profile.birthday = $(this).val();}
			});
			
			if($scope.fill_in_profile){
				
				$scope.updates.edit_profile();
				
				$scope.fill_in_profile_message = "Please take a moment to update your profile";
			}
		});
		
		if(panel.defined($scope.profile) && $scope.profile.length == 0){
			
			$scope.profile = {};
		}
	}
	
	
	$scope._cancel = function(){$('.inline-popup').hide();}
	
	$scope.popup = {
		
		businesses: function(){
		
			panel.fetch('/house/popup?p=my-businesses', function(response){
				
				var html = $(response);
				
				$compile(html)($scope);
				
				$("body").append(html);
			});
		},
		
		relationships: function(type){
			
			$scope.type_of_relationship = type.charAt(0).toUpperCase() + type.slice(1);
			
			$scope.relationships = $scope.activity[type];
			
			$("#my-friends-data").show();
			
			/*
			panel.fetch('/house/popup?p=my-friends&'+Math.random()*100, function(response){
				
				var html = $(response);
				
				$compile(html)($scope);
				
				$("body").append(html);
			});
			*/
		},
        
        friend_requests : function(){
			$("#friend-requests").show();
        },
		
		relations: function(type){
			
			$scope.type_of_relationship = type.charAt(0).toUpperCase() + type.slice(1);
			
			panel.fetch('/user/'+$scope.user.username+'?query='+type+'&json=true', function(response){
				
				$scope.relationships = response;
				
				panel.fetch('/house/popup?p=my-friends', function(popup){
				
					var html = $(popup);
					
					$compile(html)($scope);
					
					$("body").append(html);
				});
				
			}, true);
		},
		
		review_details: function(review){
			
			$scope.current_review = review;
			
			$scope.current_review_q = review.answers;
			
			$("#review-rating-breakdown").show();
		}
	}
	
	$scope.get_percentage = function(val){
		
		return Math.round((val/10)*100);
	}
	
	$scope.get_percentage_activity = function(val){
		
		var sum = 0;
		
		for(var i in $scope.activity.rating){
			
			if($scope.activity.rating[i]['size']){sum = sum + parseInt($scope.activity.rating[i]['size']);}
		}
		
		return Math.floor((parseInt(val)/sum)*100);
	}
	
	$scope.interact = {
		
		register_click_event: function(){
			
			$(document).ready(function(){
				
				$('#rewinder').unbind("click");
				
				$('#rewinder').click(function(){$scope.interact.rewind();});
			});
		},
		
		load_Ajax_Page: function(url, parameters){
			
			$scope.interact.register_click_event();
			
			$scope.ajaxParameters = !parameters ? '' : parameters;
			
			panel.fetch(url, function(response){
				
				panel.loading(false);
				
				if($scope.page_no){delete $scope.page_no;}
				
				$('body, html').animate({scrollTop: 0}, 500);
				
				var content = $(response);
				
				$compile(content)($scope);
				
				$('#ajax-viewer').html(content);
				
				$('#ajax-view-display').css({'z-index':2});
				
				$('.profile-view.other').show();
				
				$('.profile-view.basic').hide();
				
				$scope.interact.activity();
				
				if(url.indexOf('show')>-1){
					
					var showing = url.substr(url.indexOf('show=')+5);
					
					showing = showing.substr(0, showing.indexOf('&'));
					
					$scope.interact.showing_reviews(showing);
				}
				
			}, true, true);
		},
		
		ajax: function(page, params){
			
			var parameters = '&';
			
			if(params){for(var i in params){parameters += i+'='+params[i]+'&';}}
			
			if(!$scope.history){
				
				$scope.rewinding = false;
				
				$scope.history = ['/user/'+$scope.user.username];
			}
			
			$scope.active_page = page;
			
			var url = '/user/'+$scope.user.username+'/'+page+'?json=true&ajax=true'+parameters;
			
			$scope.history.push(url);
			
			$scope.interact.load_Ajax_Page(url, parameters);
		},
		
		visitor_main: function(){
			
			$('.my-slide-content').html('');
			
			$('.my-slide').hide();
			
			$('header').show();
			
			$('.my-page').show();
		},
		
		visitor_page: function(page){
			
			var url = '/user/'+$scope.user.username+'/'+page+'?json=true&ajax=true';
			
			panel.fetch(url, function(response){
				
				panel.loading(false);
				
				$('body, html').animate({scrollTop: 0}, 100);
				
				var content = $(response);
				
				$compile(content)($scope);
				
				$('.my-slide-content').html(content);
				
				$('.my-slide').show();
				
				$('header').hide();
				
				$('.my-page').hide();
				
				$scope.interact.activity();
				
				if(url.indexOf('show')>-1){
					
					var showing = url.substr(url.indexOf('show=')+5);
					
					showing = showing.substr(0, showing.indexOf('&'));
					
					$scope.interact.showing_reviews(showing);
				}
				
			}, true, true);
		},
		
		rewind: function(){
			
			if($scope.history){
				
				var last_visited = $scope.history.pop();
				
				if(!$scope.rewinding){last_visited = $scope.history.pop();$scope.rewinding=true;}
				
				if($scope.history.length > 0){
					
					$scope.interact.load_Ajax_Page(last_visited);
					
					window.history.back();
				}
				else{
					
					$scope.rewinding=false;
					
					$scope.history.push(last_visited);
					
					$('#ajax-viewer').html('');
						
					$('#ajax-view-display').css({'z-index':-100});
					
					$('.profile-view.other').hide();
					
					$('.profile-view.basic').show();
					
					window.history.back();
				}
			}
			else{
				
				window.history.back();
			}
		},
		
		activity: function(){
			
			$scope.activity = {};
			
			$scope.activity = panel.data("#init-activity");
							
			var which = panel.getParam('show').length > 0 ? panel.getParam('show') : 'all';
			
			if(which.length > 0){
				
				var all = '.review-types a', grey = '.review-types .'+which;
				
				$(all).removeClass('grey'); $(grey).addClass('grey');
			}
			if($("#handheld")){
				
				$('#ajax-view-display').css({'z-index':2});
				
				$('.profile-view.other').show();
				
				$('.profile-view.basic').hide();
				
				$scope.interact.register_click_event();
			}
			
			$(document).ready(function(){
				
				$(window).scroll(function(){
					
					var loadHeight = $(document).height() - $(window).height();
					
					if($(window).scrollTop() == loadHeight){
						
						if($scope.active_page){
							
							var page = $scope.active_page;
							
							if(!$scope.page_no){$scope.page_no = 1;$scope.fetch_more_activity = true;}
							
							if($scope.fetch_more_activity){
								
								var params = panel.getParams();
								
								var extras = !$scope.ajaxParameters ? '' : $scope.ajaxParameters;
								
								var url = '/user/'+$scope.user.username+'/'+page+'?json=true&page_no='+$scope.page_no+extras;
								
								if(params){for(var i in params){url += '&'+i+'='+params[i];}}
								
								panel.fetch(url, function(response){
									
									if(response.key){
										
										for(var i in response.data){
											
											$scope.activity[response.key].push(response.data[i]);
										}
										
										if(!$('.back_to_top').length){$('.middle').not('.top').append('<a class="back_to_top blue" style="float:right;" href="#top">Back to top</a>');}
										
										$scope.page_no++;
									}
									else if(typeof response.fetch == 'undefined'){
										
										for(var i in response){
											
											$scope.activity.push(response[i]);
										}
										
										if(!$('.back_to_top').length){$('.middle').not('.top').append('<a class="back_to_top blue" style="float:right;" href="#top">Back to top</a>');}
										
										$scope.page_no++;
									}
									else{
										
										$scope.fetch_more_activity = false;
									}
								});
							}
						}
					}
				});
			});
		},
		
		showing_reviews: function(which, load){
			
			var all = '.review-types a', grey = '.review-types .'+which;
			
			$(all).removeClass('grey'); $(grey).addClass('grey');
		},
		
		init_friends: function(){
			
			$(document).ready(function(){
				
				$('.friend-options').each(function(){
					
					$(this).blur(function(){
						
				//		$('.friend-options-bubble').addClass('hide');
					});
				});
			});
		},
		
		friend_options: function(i){
			
			var e = $("#friend-"+i.id);
			
			if(e.hasClass('hide')){e.removeClass('hide');}else{e.addClass('hide');}
		},
		
		accept_friend: function(i, is_mobile){
			
            panel.post('/incoming', {'type': 'accept_friend_request', 'id': i.request_id, 'is_friend': 1}, function (response) {

                if (response.Request == 'OK') {
                    if (is_mobile) {
                        var i = $scope.activity.indexOf(i);

                        $scope.activity.splice(i, 1);
                    } else {
                        var i = $scope.activity.friend_requests.indexOf(i);

                        $scope.activity.friend_requests.splice(i, 1);
                    }

                    $scope.q_friend_requests = $scope.q_friend_requests - 1;
                }
            });
		},
		
		search_relations: function(relationship, acts){
			
			var r = !relationship ? 'friends' : relationship.toLowerCase();
			
			var s = $scope.find_in_relationship;
			
			panel.fetch('/user/'+$scope.user.username+'?json=true&query='+r+'&search='+s, function(response){
				
				if(relationship && !acts){
					
					$scope.relationships = response;
				}
				else{
					
					$scope.activity = response;
				}
			});
		},
		
		decline_friend: function(i, is_mobile){
			
			panel.post('/incoming', {'type':'decline_friend_request','id':i.request_id,'friend_revoked':1}, function(response){
				
                if (is_mobile) {
                    var i = $scope.activity.indexOf(i);

                    $scope.activity.splice(i, 1);
                } else {
                    var i = $scope.activity.friend_requests.indexOf(i);

                    $scope.activity.friend_requests.splice(i, 1);
                }

                $scope.q_friend_requests = $scope.q_friend_requests - 1;
			});
		},
		
		update_preferences: function(){
			
			var notifications = angular.toJson($scope.activity.notifications);
			
			panel.post('/incoming', {'type':'user_settings', 'notifications':notifications}, function(response){
				
				if(response.Request == 'OK'){
					
					panel.alert("Thank you. Your preferences have been saved!");
				}
			});
		}
	};
	
	$scope.bookmark = {
		
		privacy: function(b, state){
			
			var data = {type:'manage_bookmarks', 'action':'update', id:b.id, user_id:b.user_id, 'private': state};
			
			panel.post('/incoming', data, function(response){
				
				if(response.Request == 'OK'){
					
					var i = $scope.activity.bookmarks.indexOf(b);
					
					$scope.activity.bookmarks[i]['private'] = state;
				}
			});
		},
		
		remove: function(b){
			
			panel.confirm("Are you sure you want to delete this bookmark?", function(){
				
				var data = {type:'manage_bookmarks', 'action':'delete', id:b.id, user_id:b.user_id};
				
				panel.post('/incoming', data, function(response){
					
					var i = $scope.activity.bookmarks.indexOf(b);
					
					$scope.activity.bookmarks.splice(i, 1);
					
					$scope.activity.size = parseInt($scope.activity.size) - 1;
				});
			});
		}
	}
	
	$scope.reviews = {
		
		share: function(review){
			
			var data = {type:'manage_reviews', id:review.id, action:'share'};
				
			panel.post('/incoming', data, function(response){
				
				
			});
		},
		
		edit: function(review){
			
			location.href = '/reviews/edit/'+review.id;
		},
		
		remove: function(review){
			
			panel.confirm('Are you sure you want to delete this review?', function(){
				
				var data = {type:'manage_reviews', id:review.id, action:'delete'};
				
				panel.post('/incoming', data, function(response){
					
					
				});
			});
		}
	}
	
	$scope.review_liked = function(i){$scope.activity[i].liked=1;}
	
	$scope.review_reported = function(response){
		
		for(var i in $scope.activity){
			
			if($scope.activity[i].id == response.content_id){
				
				$scope.activity[i].flagged=1;
				
				$scope.activity[i].num_kudos = parseInt($scope.activity[i].num_kudos)+1;
				
				break;
			}
		}
	}
	
	$scope.friend_requested = function(index, isFriend){
		
		$scope.relationship.request_sent = 1;
		
	}
    
    $scope.friend_accepted = function() {
        $scope.relationship.friend = 1;
        $scope.relationship.requested = 0;
    }
	
	$scope.friend_removed_activity = function(i){$scope.activity.splice(i, 1);}
    
    $scope.friend_requested_on_me = function(i){$scope.relationships[i].is_friend_request = 1;}
	
	$scope.friend_removed_on_me = function(i){$scope.relationships.splice(i, 1);}
	
	$scope.follower_blocked_on_me = function(i){$scope.relationships.splice(i, 1);}
	
	$scope.following_removed_on_me = function(i){$scope.relationships.splice(i, 1);}
	
	$scope.friend_removed = function(){delete $scope.relationship.friend;}
	
	$scope.follow_added = function(){$scope.relationship.follower = 1;$scope.relations.followers = parseInt($scope.relations.followers)+1;}
	
	$scope.follow_removed = function(){delete $scope.relationship.follower;$scope.relations.following = parseInt($scope.relations.following)+1;}
	
	$scope.updates = {
		
		edit_status: function(elem, flip){
			
			var item = "#"+elem, pen = "."+elem+"-pen", save = "."+elem+"-save";
			
			if(flip){$(pen).removeClass('hide'); $(save).addClass('hide');}
			else{$(item).focus(); $(pen).addClass('hide'); $(save).removeClass('hide');}
		},
		
		save_status: function(elem, which, event){
			
			var data = {type:'profile'}, element = $("#edit-"+which);
			
			if(event){if(event.keyCode != 13){return;}else{element.blur();element.trigger({type: 'keypress', which: 46, keyCode: 46});}}
			
			data[which] = element.text();
			
			panel.post('/incoming', data, function(response){
				
				if(response.Request == 'OK'){
					
					$scope.updates.edit_status(elem, true);
				}
			});
		},
		
		edit_profile: function(){
			
			$("#edit-profile-form").show();
		},
                
		cancel_edit: function(){
			
			$("#edit-profile-form").hide();
		},
		
		save_profile: function(){
			
			var data = $.extend({type:'profile'}, $scope.profile);
			
			// panel.alert(data); return;
			
			panel.post('/incoming', data, function(response){
				
				if(response.Request == 'OK'){
					
                    var currentDate = new Date();
                    var brithday = new Date(response.profile.birthday);

                    if (!isNaN(brithday.getFullYear())) {
                        response.profile.age = currentDate.getFullYear() - brithday.getFullYear();
                    }
					$scope.profile = response.profile;
                    
					
					$("#edit-profile-form").hide();
				}
			});
		}
	};
	
	$scope.community_keywords = function(str){
		
		var tags = str.split(',');
		
		var html = '';
		
		for(i in tags){
		
			html = html+ '<a href="/search/community/'+$scope.url_string(tags[i])+'" target="_blank" class="keyword">'+tags[i]+'</a>';
		}
		
		return html;
	}
	
	$scope.limit = function(str){
	
		return str.substr(0,150);
	}
	
	$scope.url_string = function(str){
	
		return str.replace(/\s/g, "-").toLowerCase();
	}
	
	$scope.changePic = function(){$("#fileupload").click();}
	
	$scope.uploadPic = function(data){
            var FileUploadPath = document.getElementById("fileupload").value;
            var Extension = FileUploadPath.substring(
            FileUploadPath.lastIndexOf('.') + 1).toLowerCase();

            if (Extension == "gif" || Extension == "png" || Extension == "bmp" || Extension == "jpeg" || Extension == "jpg") {
		var photo = new FormData();
		
		for(var i in data){
		
			photo.append('avatar', data[i]);
		}
		
		panel.upload('/incoming/avatar', photo, function(response){
		
			if(response.Request == 'OK'){
				
				$('.profile-photo').css("background-image", 'url('+panel.__staticserv()+'/images/profile/'+response.profile_pic+')');
				$('.menu-profile-pic').css("background-image", 'url('+panel.__staticserv()+'/images/profile/'+response.profile_pic+')');
				
				$scope.user.profile_pic = response.profile_pic;
				
				
			}else if(response.Request == 'error'){
			panel.alert(response.error);	
			}
		}, true);
            } 
            else {
                alert("Photo only allows file types of GIF, PNG, JPG, JPEG and BMP. ");

            }
	}

	$scope.edit_profile = function(){
	
		$('.edit').removeClass('noedit').removeAttr('readonly');
		$("#edit_profile").hide();
		$("#save_profile").show();
		$("#editor").show();
		$("#viewer").hide();
	}
	
	$scope.save_profile = function(){
	
		$('.edit').addClass('noedit').attr('readonly',true);
		$("#edit_profile").show();
		$("#save_profile").hide();
		$("#editor").hide();		
		$("#viewer").show();
		
		$scope.profile.type = 'profile';
		
		panel.post('/incoming', $scope.profile, function(response){
			
			if(response.Request == 'OK'){
			
			}
		});
	}
	
	$scope.update_settings = function(){
		
		var data = {type:'user_settings', notifications:angular.toJson($scope.info.notifications)};
		
		$scope.error = '';
		
		if(typeof $scope.password != 'undefined'){
			
			if(typeof $scope.password.old == 'undefined' || $scope.password.old.trim() == ''){
				
				$scope.error = 'Your did not enter your current password.';
			}
			
			if($scope.error == '' && $scope.password.reset.length < 8){
			
				$scope.error = 'Your new password is too short. Minimum 8 characters.';
			}
			else if($scope.error == '' && $scope.password.reset != $scope.password.confirm){
			
				$scope.error = 'Your new passwords do not match.';
			}
			
			if($scope.error == ''){
				
				data.password = $scope.password;
			}
			else{
				
				return;
			}
		}
		
		panel.post('/incoming', data, function(response){
			
			if(response.Request == 'OK'){
				
				panel.alert("Your changes have been saved!!!");
			}
			else{
				
				panel.alert(response.Error);
			}
		});
	}
	
}]);

/*Session and Search handling functions and data*/
ascoreControllers.controller('SessionController', ['$scope','panel', '$timeout', function($scope, panel, $timeout){

	/*static saved
	$scope.categories = [{"id":"12","name":"Health \u0026 Beauty","mobile_name":"Health","parent":"0","order":"1","page_title":"","page_question":"","homepage":"1","markeddeleted":"0","type":"Main","image":"beauty.jpg","icon":"icon-health-white-30px.png","icon_main":"health_beauty.png","icon_rollover":"health_beauty_selected.png","s_name":"Health \u0026 Beauty 12","url_string":"health-beauty-12"},{"id":"2","name":"Automotive","parent":"0","order":"2","page_title":"","page_question":"","homepage":"1","markeddeleted":"0","type":"Main","image":"automobile.jpg","icon":"icon-auto-white-30px.png","icon_main":"automobile.png","icon_rollover":"automobile_selected.png","s_name":"Automotive 2","url_string":"automotive-2"},{"id":"17","name":"Computers","parent":"0","order":"3","page_title":"","page_question":"","homepage":"1","markeddeleted":"0","type":"Main","image":"computers.jpg","icon":"icon-computers-white-30px.png","icon_main":"computers.png","icon_rollover":"computers_selected.png","s_name":"Computers 17","url_string":"computers-17"},{"id":"3","name":"Doctors","parent":"0","order":"4","page_title":"","page_question":"","homepage":"1","markeddeleted":"0","type":"Main","image":"doctor.jpg","icon":"icon-doctor-white-30px.png","icon_main":"doctors.png","icon_rollover":"doctors_selected.png","s_name":"Doctors 3","url_string":"doctors-3"},{"id":"8","name":"Childcare","parent":"0","order":"5","page_title":"","page_question":"","homepage":"1","markeddeleted":"0","type":"Main","image":"childcare.jpg","icon":"icon-childcare-white-30px.png","icon_main":"childcare.png","icon_rollover":"childcare_selected.png","s_name":"Childcare 8","url_string":"childcare-8"},{"id":"10","name":"Education \u0026 Training","mobile_name":"Education","parent":"0","order":"6","page_title":"","page_question":"","homepage":"1","markeddeleted":"0","type":"Main","image":"education.jpg","icon":"icon-education-white-30px.png","icon_main":"education.png","icon_rollover":"education_selected.png","s_name":"Education \u0026 Training 10","url_string":"education-training-10"},{"id":"14","name":"Event Planning","parent":"0","order":"7","page_title":"","page_question":"","homepage":"1","markeddeleted":"0","type":"Main","image":"eventplan.jpg","icon":"icon-eventplan-white-30px.png","icon_main":"event_planning.png","icon_rollover":"event_planning_selected.png","s_name":"Event Planning 14","url_string":"event-planning-14"},{"id":"78","name":"Restaurant","parent":"0","order":"8","page_title":"","page_question":"","homepage":"1","markeddeleted":"0","type":"Main","image":"restaurants.jpg","icon":"icon-restaurants-white-30px.png","icon_main":"restaurants.png","icon_rollover":"restaurants_selected.png","s_name":"Restaurant 78","url_string":"restaurant-78"},{"id":"13","name":"Legal","parent":"0","order":"9","page_title":"","page_question":"","homepage":"1","markeddeleted":"0","type":"Main","image":"law.jpg","icon":"icon-law-white-30px.png","icon_main":"legal.png","icon_rollover":"legal_selected.png","s_name":"Legal 13","url_string":"legal-13"},{"id":"4","name":"Financial","parent":"0","order":"10","page_title":"","page_question":"","homepage":"1","markeddeleted":"0","type":"Main","image":"financial.jpg","icon":"icon-financial-white-30px.png","icon_main":"financial.png","icon_rollover":"financial_selected.png","s_name":"Financial 4","url_string":"financial-4"},{"id":"7","name":"Real Estate","parent":"0","order":"11","page_title":"","page_question":"","homepage":"1","markeddeleted":"0","type":"Main","image":"realestate.jpg","icon":"icon-realestate-white-30px.png","icon_main":"real_estate.png","icon_rollover":"real_estate_selected.png","s_name":"Real Estate 7","url_string":"real-estate-7"},{"id":"5","name":"Home \u0026 Renovation","mobile_name":"Home Reno","parent":"0","order":"12","page_title":"","page_question":"","homepage":"1","markeddeleted":"0","type":"Main","image":"reno.jpg","icon":"icon-reno-white-30px.png","icon_main":"home_renovation.png","icon_rollover":"home_renovation_selected.png","s_name":"Home \u0026 Renovation 5","url_string":"home-renovation-5"}];
	*/
	$scope.toggleLocation = function(x){
		
		(x == 1) ? $("#location-bar").removeClass('hide') && 
					$(".close-location").removeClass('hide') && 
					$("#global-menu").addClass('hide') && 
					$('.logo-png').addClass('hide') && 
					$('.ascore-search').removeClass('hide')
				 : $("#location-bar").addClass('hide') && 
				 $(".close-location").addClass('hide') && 
				 $("#global-menu").removeClass('hide') && 
				 $('.logo-png').removeClass('hide') && 
				 $('.ascore-search').addClass('hide');
	}
	
	$scope.setLocation = function(){
		
		
		if($scope.search.place.locality && $scope.search.place.locality.length > 0){
			
			$scope.search.place.label = $scope.search.place.city+', '+$scope.search.place.region;
		}
		else if($scope.search.place.city && $scope.search.place.city.length > 0){
			
			$scope.search.place.label = $scope.search.place.city;
			
			if($scope.search.place.region.toLowerCase() != $scope.search.place.city.toLowerCase()){
				
				$scope.search.place.label = $scope.search.place.label+', '+$scope.search.place.region;
			}
		}
		else if($scope.search.place.region && $scope.search.place.province.length > 0){
			
			$scope.search.place.label = $scope.search.place.province+', '+$scope.search.place.countryCode;
		}
		else{
			
			$scope.search.place.label = $scope.search.place.country;
		
		}
	}
	
	$scope.setSearchLocation = function(response){
		
		$scope.search = {place:response};
		
		$scope.setLocation();
	}
	
	
	$scope.load_notifications = function(){
		
		if($scope.session){
			
			panel.fetch('/house/data?query=notifications', function(response){
				
				if(response.Request == 'OK'){$scope.notifications = response.notifications;}
			});
		}
	}
	
	$scope.init = function(){
		
		var data = panel.data(); for(i in data){$scope[i] = data[i];}
	//	panel.alert(data);
		$scope.load_notifications();
        if ($scope.queryLocation) {
            $scope.search.place = $scope.queryLocation[0];
        }
        panel.register_session_callback(function(){
			
			panel.fetch('/access/session', function(response){
				
				if(response.Request == 'OK'){
					
					$scope.session = response.session;
					if(typeof response.session.username != 'undefined')
                    {
					    $("#sign-in").hide() && $("#signed-in").show();
                    }
					
					$scope.load_notifications();
				}
			});
		});
		
		if(typeof $scope.search == 'undefined'){
			
			if($scope.search.place){
				
				$scope.setSearchLocation($scope.search.place);
			}
			else{
				
				var url = 'http://ip-api.com/json/';
				
				panel.fetch(url, function(response){
					
					$scope.setSearchLocation(response);
					
				}, true);
			}
			$scope.search = {place:panel.getPlace()};
		}
		else{
			
			if($scope.search.place){
				
				$scope.setSearchLocation($scope.search.place);
			}
			else{
				if(typeof $scope.search== 'undefined'){
				
				$scope.setLocation();
				}
			}
		}
		
		$(document).ready(function(){
		
			var window_width = $(window).width();
			
			var size = Math.ceil((window_width-1000)/2);
			
			if($('body').attr('id') == "handheld"){
				
				size = window_width; $scope.isMobile = true; /* for mobile screens fill page */
				
				$('.slider-button').each(function(){
					
					var btn = $(this);
					
					var target = $('.'+btn.attr('target'));
					
					btn.click(function(){
						
						$('body, html').animate({scrollTop: 0}, 100);
						
						target.addClass('slide-active');
							
						target.animate({'left': '0px'}, 400);
					});
				});
				
				$('.slide-page').each(function(){
					
					var slide = $(this);
					
					slide.css({height:$('body').height()+'px', left:'-200%'});
					
					slide.find('.slide-back').click(function(){
						
						if(slide.hasClass('slide-active')){
							
							slide.removeClass('slide-active');
							
							slide.animate({'left': size*-1+'px'}, 400, 'easeOutQuint');
						}
					});
				});
			}
			else if(size<180){ size = 225;}
			
			var openMenu = function(m){
				
				if(!$scope.isMobile){
					
					$("body").append("<div class='overlay'></div>");$('.overlay');
					
					$(".overlay").click(function(){closeMenu(m);m.removeClass('open');});
				}
				
				m.animate({'left': '0px'}, 200);/*'easeOutBack'*/
				
				$("#mobile-close-menu").removeClass('hide'); /* for mobile */
				
				/* $("#global-menu").addClass('hide');  for mobile */
				
				$('.no-menu-display').addClass('hide');
				
				$('.search-with-menu').removeClass('hide');
				
				document.body.style.overflow = 'hidden';
			}
			
			var closeMenu = function(m){
				
				if(!$scope.isMobile){
					
					$(".overlay").remove();
				}
				
				m.animate({'left': size*-2+'px'}, 400, 'easeOutQuint');
				
				$("#mobile-close-menu").addClass('hide'); /* for mobile */
				
				/* $("#global-menu").removeClass('hide');  for mobile */
				
				$('.no-menu-display').removeClass('hide');
				
				$('.search-with-menu').addClass('hide');
				
				document.body.style.overflow = 'auto';
			}
			
			var m = $("#menubar");
			
			m.css({'width':size+'px',left:size*-2+'px',display:'block'});
			
			$("#global-menu").click(function(){
				
				
				if(!m.hasClass("open")){m.addClass("open");openMenu(m);}else{closeMenu(m);m.removeClass('open');}
			});
			
			$("#mobile-close-menu,#close-menu").click(function(){closeMenu(m);m.removeClass('open');});
			
			$('.rollover').each(function(){$(this).mouseover(function(){$(this).siblings().show(); $(this).hide();});});
			
			$('.xrollover').each(function(){$(this).mouseleave(function(){$(this).siblings().show(); $(this).hide();});});
			
			$('.x-link-rollover').each(function(){
				
				$(this).mouseover(function(){
					
					$(this).children('.x-icon').hide();$(this).children('.x-icon-hover').show();
				});
				
				$(this).mouseleave(function(){
					
					$(this).children('.x-icon-hover').hide();$(this).children('.x-icon').show();
				});
			});
		});
	
        $scope.toggleMenu = function (item, isOpen) {
            if (isOpen) {
                ($(item).hasClass('invisible') && $(item).removeClass('invisible') && $(item).show())
            } else {
                (!$(item).hasClass('invisible') && $(item).addClass('invisible') && $(item).hide());
            }
        }
	
		$scope.query = panel.getParam('t') == 'category' ? '' : panel.getParam('q');	
	
        if ($scope.type == 'user') {
            $scope.query = '';
        }
        
		$scope.do_search = function(event){
		
			if(typeof event != 'undefined' && event.keyCode != '13'){return;}
			
		/*	window.location.href="/search?t=business&q="+$scope.query+"&loc="+$scope.search.place.label;*/
		//	panel.alert($scope.search);return;
        
            if (!$scope.locationValid()){
                panel.alert("Invalid location. Please select location from the list");
                return;
            }
			
			if(typeof $scope.search.place.locality != 'undefined' && $scope.search.place.locality != null && $scope.search.place.locality.length > 0){
				
				window.location.href="/search?t=business&q="+$scope.query+"&c="+$scope.search.place.locality+"&pc="+$scope.search.place.region;
			}
			else if(typeof $scope.search.place.city != 'undefined' && $scope.search.place.city != null && $scope.search.place.city.length > 0){
				
				window.location.href="/search?t=business&q="+$scope.query+"&c="+$scope.search.place.city+"&pc="+$scope.search.place.region;
			}
			else if(typeof $scope.search.place.province != 'undefined' && $scope.search.place.province != null && $scope.search.place.province.length > 0){
				
				window.location.href="/search?t=business&q="+$scope.query+"&p="+$scope.search.place.province+"&cc="+$scope.search.place.country_code;
			}
			else{
				
				window.location.href="/search?t=business&q="+$scope.query+"&g="+$scope.search.place.country;
			}
		}
		
		$scope.do_search = function(event, is_location){
                
			var x = (typeof $scope.query != 'undefined' && $scope.query.trim().length>0) ? "?q="+$scope.query : '';

			if(typeof event != 'undefined' && event.keyCode != '13'){return;}
            
            if (!$scope.locationValid()){
                panel.alert("Invalid location. Please select location from the list");
                return;
            }

			if(is_location){window.location.href="/search/"+$scope.search.place.label.toLowerCase()+x;return;}

			if(typeof $scope.search.place.locality != 'undefined' && $scope.search.place.locality != null && $scope.search.place.locality.length > 0){
				
                    var l = $scope.search.place.locality.toLowerCase(), r = $scope.search.place.region.toLowerCase();

				window.location.href="/search/"+l+"-"+r+x;
			}
			else if(typeof $scope.search.place.city != 'undefined' && $scope.search.place.city != null && $scope.search.place.city.length > 0){

                    var c = $scope.search.place.city.toLowerCase(), r = $scope.search.place.region.toLowerCase();

				window.location.href="/search/"+c+"-"+r+x;
			}
			else if(typeof $scope.search.place.province != 'undefined' && $scope.search.place.province != null && $scope.search.place.province.length > 0){

                    var p = $scope.search.place.province.toLowerCase(), c = $scope.search.place.country_code.toLowerCase();

				window.location.href="/search/"+p+"-"+c+x;
                }
			else{
            
				window.location.href="/search/"+$scope.search.place.country.toLowerCase()+x;
                }
            }
            
            $scope.locationValid = function(){
                var newLabel = $scope.search.place.city + ',' + $scope.search.place.region;
				var newcode  = '';
				split_array	  =  $scope.search.place.label.split(",");
				if(split_array.length>2){
				
				newcode		=	split_array[1]+','+split_array[2];
					
				}
			
                if (newLabel == $scope.search.place.label) {
                    return true;
                }
				
				if($.trim(newLabel)==$.trim(newcode)){
				
				return true;	
				}
				
				
                return false;
            }
	}
	
	$scope.rewind = function(){window.history.back();}
	
	$scope.init_homepage = function(){
		
		$scope.title = "THE BEST WAY TO FIND GREAT LOCAL BUSINESSES";
	
		$scope.autoload = false;
		
		$scope.toggleLocation = function(x){
			
			(x == 1) ? $("#location-bar").removeClass('hide') && $(".close-location").removeClass('hide') && $("#global-menu").addClass('hide') && $('.logo-png').addClass('hide') && $('.ascore-search').removeClass('hide')
					 : $("#location-bar").addClass('hide') && $(".close-location").addClass('hide') && $("#global-menu").removeClass('hide') && $('.logo-png').removeClass('hide') && $('.ascore-search').addClass('hide');
		}
				
		$scope.load_mobile_category = function(cat){
			
				if(typeof cat != 'object'){
					
					for(var i in $scope.categories){
						
						if(parseInt($scope.categories[i]['id']) == parseInt(cat)){
							
							cat = $scope.categories[i]; break;
						}
					}
				}
				
				$scope.category = cat;
				
				panel.fetch('/data?query=sub-categories&id='+cat.id, function(response){
					
					$scope.sub_categories = response;
					
					$("#sub-cat-menu").animate({'left': '0%'}, 400);
				}, true);
			}
			
		$scope.hide_mobile_category = function(){$("#sub-cat-menu").animate({'left': '-200%'}, 400);}
		
		$scope.get_preview_cat_url = function(i, context){
			
			if(context == 'super'){
				
				return "/search?t=category&te="+panel.url_string(i.cat_parent)+"-"+i.cat_parent_id+"&q=&c="+i.city+"&pc="+i.province;
			}
			else{
				
				if(i.cat_parent == null){
					
					return "/search?t=category&te="+panel.url_string(i.category)+"-"+i.cat_id+"&q=&c="+i.city+"&pc="+i.province;
				}
				else{
					
					return "/search?t=category&te="+panel.url_string(i.cat_parent)+"-"+i.cat_parent_id+"&ste="+panel.url_string(i.category)+"-"+i.cat_id+"&q=&c="+i.city+"&pc="+i.province;
				}
			}
		}
		
		$scope.get_cat_url = function(cat, subcat){
			
			if(cat){
				
				var subcat_url = (typeof subcat == 'undefined') ? '' : '&ste='+subcat.id;
				
				var url = 'search?t=category&te='+cat.id+subcat_url;
				
				url = 'search/'+panel.url_string($scope.search.place.label)+'/'+cat.url;
				
				if(subcat_url != ''){
					
					url = url+'/'+subcat.url;
				}
				/*
				var params = {};
				
				if($scope.search.place.locality != null && $scope.search.place.locality.length > 0){
						
					params = {q:'',l:$scope.search.place.locality,c:$scope.search.place.city,pc:$scope.search.place.region}
				}
				else if($scope.search.place.city != null && $scope.search.place.city.length > 0){
					
					params = {q:'',c:$scope.search.place.city,pc:$scope.search.place.region}
				}
				else if($scope.search.place.province != null && $scope.search.place.province.length > 0){
				
					params = {q:'',pc:$scope.search.place.region,cc:$scope.search.place.country_code}
				}
				else{
					
					params = {q:cat.url_string,g:$scope.search.place.country}
				}
				
				for(var i in params){
					
					url = url + '&' + i + '=' + params[i];
				}
				*/
				return url;
			}
		}
		
		$scope.rollover = function(id, show){
			
			if(show){
				$("#"+id+'_on').hide();
				$("#"+id+'_off').show();
				
			}
			else{
				$("#"+id+'_off').hide();
				$("#"+id+'_on').show();
			}
		}
		/*
		$scope.$watch("sub_categories", function(){
			
			var heights = [];
					
			$(".main-sub-cats ul").each(function(){heights.push($(this).height());});
		});*/
		
		$scope.load_category = function(cat){
			
			if(typeof cat != 'object'){
					
				for(var i in $scope.categories){
					
					if(parseInt($scope.categories[i]['id']) == parseInt(cat)){
						
						cat = $scope.categories[i]; break;
					}
				}
			}
			
			var elem = $("#cat_"+cat.id);
			
			if(!elem.hasClass('hightlighted')){
					
				$("#main-cat-display td").removeClass('hightlighted');
				
				elem.addClass('hightlighted');
				
				$scope.category = cat;
					
				$scope.sub_categories = [];
				
				var i = 0;
				
				$scope.see_more_cats_href = '';
				
				var response = cat.subcats.slice(0, cat.subcats.length);
				
				if(cat.subcats.length == 40){
					
					$scope.see_more_cats_href = $scope.get_cat_url(cat);
				}
				
				var multiple = Math.ceil(response.length/5);
				
				while (response.length > 0){

				  $scope.sub_categories[i++] = response.splice(0, multiple);
				}
					
				setTimeout(function(){$("#main-cat-listing").slideDown();}, 300);
			}
			else{
				
				$scope.sub_categories = [];
				
				$("#main-cat-listing").slideUp(200);
				
				elem.removeClass('hightlighted');
			}
			/*
			if(elem.hasClass('hightlighted')){
				
				
				panel.fetch('/data?query=sub-categories&id='+cat.id, function(response){
				
					$scope.category = cat;
					
					$scope.sub_categories = [];
					
					var i = 0;
					
					var multiple = Math.ceil(response.length/5);
					
					while (response.length > 0){

					  $scope.sub_categories[i++] = response.splice(0,multiple);
					}
					
					$("#main-cat-listing").slideUp(200);
						
					$("#main-cat-listing").slideDown();
				}, true);
			}
			else{
			
				$("#main-cat-listing").slideUp(200);
				
				elem.removeClass('hightlighted');
			}
			*/
			if($scope.autoload){
				
			}
			else{
			
				$scope.category = cat;
				$scope.autoload = true;
			}
		}
		
		
		$scope.load_category_single = function(cat){
			
			if(typeof cat != 'object'){
					
				for(var i in $scope.categories){
					
					if(parseInt($scope.categories[i]['id']) == parseInt(cat)){
						
						cat = $scope.categories[i]; break;
					}
				}
			}
			
			var elem = $("#cat_"+cat.id);
			
			if(!elem.hasClass('hightlighted')){
					
				$("#main-cat-display td").removeClass('hightlighted');
				
				elem.addClass('hightlighted');
				
				$scope.category = cat;
					
				$scope.sub_categories = [];
				
				var i = 0;
				
				$scope.see_more_cats_href = '';
				
				var response = cat.subcats.slice(0, cat.subcats.length);
				
				if(cat.subcats.length == 40){
					
					$scope.see_more_cats_href = $scope.get_cat_url(cat);
				}
				
				var multiple = Math.ceil(response.length/5);
				
				while (response.length > 0){

				  $scope.sub_categories[i++] = response.splice(0, multiple);
				}
					
				setTimeout(function(){$("#main-cat-listing").slideDown();}, 300);
			}
			else{
				
				$scope.sub_categories = [];
				
				$("#main-cat-listing").slideUp(200);
				
				elem.removeClass('hightlighted');
			}
			/*
			if(elem.hasClass('hightlighted')){
				
				
				panel.fetch('/data?query=sub-categories&id='+cat.id, function(response){
				
					$scope.category = cat;
					
					$scope.sub_categories = [];
					
					var i = 0;
					
					var multiple = Math.ceil(response.length/5);
					
					while (response.length > 0){

					  $scope.sub_categories[i++] = response.splice(0,multiple);
					}
					
					$("#main-cat-listing").slideUp(200);
						
					$("#main-cat-listing").slideDown();
				}, true);
			}
			else{
			
				$("#main-cat-listing").slideUp(200);
				
				elem.removeClass('hightlighted');
			}
			*/
			if($scope.autoload){
				
			}
			else{
			
				$scope.category = cat;
				$scope.autoload = true;
			}
		}
		
		$scope.init_home = function(){
			/*
			var i = $scope.categories.length/2;
			
			$scope.categories_1 = $scope.categories.slice(0, i);
			
			$scope.categories_2 = $scope.categories.slice(i);
			
			$scope.load_category($scope.categories[0]);
			*/
			$(document).click(function(e){
			
				var container = $("#main-cat-listing"), autocomplete = $(".ui-autocomplete");
				
				if (!container.is(e.target) && container.has(e.target).length === 0 && !autocomplete.is(e.target) && autocomplete.has(e.target).length === 0){
				
					$("#main-cat-listing").slideUp(200);
				}
			});
		}
		
		$scope.init_home();
	}
	
	$scope.carousel = function(){
				 
		if(!$scope.carousel_counter){
			
			$scope.carousel_counter = 0;
			
			$scope.fetch_nomore = false;
			
			$scope.old_items = [];
		}
		
		var animate = function(){
			
			var delay = 500;
			
			if($scope.fetch_nomore){
				
				if($scope.old_items.length > 0){
					
					$scope.previews.push($scope.old_items.shift());
					
					$('.preview-slider').animate({left:'-=340', width:'+=340'}, delay, 'swing', function(){});
				}
				else{
					
					var first = $('.preview-slider .item').first();
					
					var clone = first.clone();
					
					clone.appendTo('.preview-slider');
					
					clone.find('.business-tooltip').mouseover(function(){panel.tipBusiness($(this).attr('business'));});
					clone.find('.business-tooltip').mouseout(function(){panel.tipBusiness($(this).attr('business'));});
					clone.find('.user-tooltip').mouseover(function(){panel.tipUserScore($(this).attr('business'));});
					clone.find('.user-tooltip').mouseout(function(){panel.tipUserScore($(this).attr('business'));});
					clone.find('.user-tooltip-info').mouseover(function(){panel.tipUserInfo($(this).attr('business'));});
					clone.find('.user-tooltip-info').mouseout(function(){panel.tipUserInfo($(this).attr('business'));});
					/*clone.find('.review-details').click(function(){$scope.pop_review_details()});*/
					$('.preview-slider').animate({left:'-=340', width:'+=340'}, delay, 'swing', function(){
				
						$(this).css({left:'+=340', width:'-=340'});
						
						first.remove();
					});
					
				}
			}
			else{
			 
				$scope.old_items.push($scope.previews.shift());
				
				$('.preview-slider').animate({left:'-=340', width:'+=340'}, delay, 'swing', function(){});
			}
		}
		
		if(!$scope.fetch_nomore){
			
			$scope.carousel_counter++;
			
			panel.fetch('/house/previews?offset='+$scope.carousel_counter*3, function(response){
				
				if(response.Request == 'OK'){
					
					for(var i in response.previews){
						
						$scope.previews.push(response.previews[i]);
					}
				}
				else{
					
					$scope.fetch_nomore = true;
				}
				
				animate();
			});
		}
		else{
			
			animate();
		}
	}
	
	$scope.pop_review_details = function(review){
		
		$scope.current_review = review;
		
		$scope.current_review_rating = 0;
		
		$scope.current_review_q = review.answers;
			
		for(var i in $scope.current_review_q){
			
			$scope.current_review_rating = $scope.current_review_rating + parseInt($scope.current_review_q[i].answer);
		}
		
		$scope.current_review_rating = ($scope.current_review_rating/$scope.current_review_q.length)*10;
		
		$("#review-rating-breakdown").show();
	}
	
	$scope.evaluate_question = function($event){
		
		var str = $scope.new_question;
		
		var prompt = $(".question-mark-bubble");
		
		if($event && $event.keyCode != 13){
			
			!prompt.hasClass('hide') && prompt.addClass('hide');
			
			return;
		}
		
		if(str.charAt(str.trim().length-1) != '?'){
			
			prompt.removeClass('hide');
			
			return;
		}
		
		location.href = '/community/ask?question='+str;
	}
}]);


ascoreControllers.controller('SearchController', ['$scope','panel', '$compile', function($scope, panel, $compile){

        $scope.serach_member = function (){
             console.log('okko');
           
        }

	$scope.init = function(){
	
		$scope.params = {};
		
		$scope.ratings = [{param:'rt',v:'desc',text:'Highest',checked:false},{param:'rt',v:'asc',text:'Lowest',checked:false}]; /*excluded {param:'fr',v:1,text:'Reviewed by Friends only'}*/
		
		$scope.category_type = panel.getParam('te') != '' ? 'ste' : 'te';
		
		var data = panel.data();
		
		$scope.stats = {};
		
		for(var i in data){
		
			$scope[i] = data[i];
		}
		
		if(typeof $scope.subcats != 'undefined'){
			
			$scope.mincats = $scope.subcats.splice(0, 4);
		}
		
		if(typeof $scope.hoods != 'undefined'){
			
			$scope.minhoods = $scope.hoods.splice(0, 4);
		}
		
		if($scope.results.length == 2){
		
			var stats = $scope.results[1];
			
			for(var i in stats){
				
				var k = stats[i]['Variable_name'];
				
				var v = stats[i]['Value'];
				
				$scope.stats[k] = v;
			}
			
			if(typeof $scope.stats.total != 'undefined'){
				
				$scope.results = $scope.results[0];
			}
			
		}
		
		$scope.per_page = 10;
		
		$scope.pages = Math.ceil($scope.stats.total/$scope.per_page);
		
		$scope.page_no = 1;
		
		if($scope.pages > 1){
			
			var p = parseInt(panel.getParam('page'));
			
			if(p){$scope.page_no = p;}
			
			$scope.next = 2;
			
			if(p < $scope.pages){$scope.next = p+1;$scope.prev = p-1;}
			else if(p == $scope.pages){delete $scope.next;$scope.prev = p-1;}
			
			if($scope.pages > 1){
				
				var num_buttons = 5;
				
				var modulo = $scope.page_no%num_buttons;
				
				var min_page = 0; var max_page = 0;
				
				if($scope.page_no < num_buttons && num_buttons <= $scope.pages){
					
					min_page = 1; max_page = num_buttons;
				}
				else{
					
					min_page = $scope.page_no - (num_buttons-3);
					
					if(($scope.pages - $scope.page_no) < num_buttons && (min_page + num_buttons) < $scope.pages){
						
						max_page = min_page+num_buttons;
					}
					else if((min_page + num_buttons) > $scope.pages){
						
						min_page = $scope.pages - (num_buttons - 1);
						
						max_page = $scope.pages;
					}
					else{
						
						max_page = $scope.page_no + 2;
					}
				}
				
				$scope.page_buttons = [];
				
				for(var i = 0 ; i < num_buttons ; i++){
					
					if(min_page <= max_page){
						
						$scope.page_buttons.push(min_page++);
					}
				}
			}
		}
		
		$scope.searchUrl = $scope.refine_search("", "", true);
		
		
		$(document).ready(function(){
			
			$(".custom-checkbox").each(function(){
				
				$(this).click(function(){$(this).children("input").click();});
			});
		});
            
           
	}
        
	
	$scope.init_mobile = function(){
		
		$(document).ready(function(){
			
			$('.btn-filter').each(function(){
				
				$(this).click(function(){
					
					if(!$(this).hasClass('exposed')){
						
						$(this).addClass('exposed');
					}
					else{
						
						$(this).removeClass('exposed')
					}
				});
			});
			
			$('.refine-title').each(function(){
				
				$(this).click(function(){
					
					if(!$(this).hasClass('exposed')){
						
						$(this).addClass('exposed');
						
						$(this).find('.fa-angle-down').addClass('hide').removeClass('show');
						
						$(this).find('.fa-angle-up').addClass('show').removeClass('hide');
						
						$(this).next().show();
					}
					else{
						
						$(this).removeClass('exposed');
						
						$(this).find('.fa-angle-down').addClass('show').removeClass('hide');
						
						$(this).find('.fa-angle-up').addClass('hide').removeClass('show');
						
						$(this).next().hide();
					}
					
				});
			});
			
			var height = $(window).height();
			
		//	$('.main-content-area').css({'min-height':height+'px'});
			
			$('#map-canvas').height(height-150);
		});
	}
	
	$scope.mobile = {
		
		toggle_view : function(view, event){
			
			var view = $('.view-'+view);
			
			var target = (event && event.target) ? $(event.target) : false;
			
			if(!view.hasClass('displaying')){
				
				$('.search-view').removeClass('displaying');
				
				$('.search-view').css({'z-index':0, 'display':'none'});
				
				view.addClass('displaying');
				
				view.css({'z-index':1, 'display':'block'});
				
				target && target.html(target.attr('back'));
			}
			else{
				
				target && target.html(target.attr('original'));
				
				view = $('.view-results');
				
				$('.search-view').removeClass('displaying');
				
				$('.search-view').css({'z-index':0, 'display':'none'});
				
				view.css({'z-index':1, 'display':'block'});
			}
		}
	}
	
	$scope.set_selected_page = function(page){if($scope.page_no == page){return 'selected';}}
	
	$scope.get_result_rating = function(r){
		
		return '<img src='+panel.__staticserv()+'"/public/images/ratings-icon-circle-version.png">';
	}
	
	$scope.get_info_window = function(i){
		
		var r = $scope.results[parseInt(i)];
		
		var html = '<div class="inline-block"><div><a style="font-size:14px;" href="/biz/'+panel.url_string(r.name)+'/'+r.id+'" class="bold blue">'+r.name+'</a></div>';
		html += '<div>'+$scope.get_result_rating(r)+' <span class="business-rating" class="inline-block" >'+r.rating*10+'%</span> '+(r.review_count == null ? 0 : r.review_count)+' reviews</div>';
		html += '<div><small class="block">'+r.street_line_1+' '+r.street_line_2+' '+panel.postal_code(r.postal_code)+'</small>';
		html += '<small class="block">'+r.city+' '+r.province_code+'</small></div></div>';
		/*html += '<div><a class="blue" href=""><small>Get Directions</a></small></div>';*/
		
		return html;
	}
	
	$scope.toggle_filters = function(){
		
		var sf = $('.show-filters'), sh = $('.hide-filters'), filters = $('.search-filters');
		
		filters.toggle();
		
		sf.hasClass('hide') ? sf.removeClass('hide') && sh.addClass('hide') : sf.addClass('hide') && sh.removeClass('hide');
	}
	
	$scope.my_location = function(){
		
		var city = panel.getParam('c'), province = panel.getParam('pc');
		
		if(city && province){
			
			return city+', '+province;
		}
	}
	$scope.update = function(param, item, replace){
		
		var spliced = false;
		
		var val = typeof($scope.params[param]) != 'undefined' ? $scope.params[param] : panel.getParam(param);
		
		var items = val.split(',');
		
		for(i in items){
			
			if(items[i] == item){
				
				items.splice(i, 1);
				
				spliced = true;
				
				break;
			}
		}
		
		if(!spliced){
			
			items.push(item);
		}
		
		if(replace){
			
			$scope.params[param] = item.replace(/^,/,'');
		}
		else{
			
			$scope.params[param] = items.join().replace(/^,/,'');
		}
		
		$scope.searchUrl = $scope.refine_search(param, $scope.params[param], true);
	
	}
	
	$scope.getValues = function(key){
		
		var val = typeof($scope.params[key]) != 'undefined' ? $scope.params[key] : panel.getParam(key);
		
		if(val.length>0) return val.split(',');
		
		return [];
		
	}
	
	$scope.check_mark_cat = function(item, param){
		
		var params = $scope.getValues(param);
		
		return params.indexOf(item) > -1 ? true : false;
	}
	
	$scope.check_mark_hood = function(item, param){
		
		var params = $scope.getValues(param);
		
		return params.indexOf(item) > -1 ? true : false;
	}
	
	$scope.reload_city = function(){
		
		if($scope.search){
			
			$scope.update('city', $scope.search.city.name, true);
			
			$scope.update('lc', $scope.search.city.province, true);
			
			location.href = $scope.searchUrl;
		}
	}
	
	$scope.display = function(item){
		
		var e = $(item);
		
		if(!e.hasClass('exposed')){ e.addClass('exposed');e.show();}
		
		else{ e.removeClass('exposed');e.hide();}
	}
	
	$scope.done = {};
	
	$scope.setSelectedState = function(key, val, obj, con){
		if(typeof obj == 'object'){obj.selected = "0";}
		
		var v = panel.getParam(key);
		
		var vals = v.split(',');
		
		if(typeof vals == 'object'){
			
			for(var i=0;i<vals.length;i++){
				if(val == vals[i]){
					if(typeof obj == 'object'){
                                            obj.selected = "1";
                                            obj.checked = true;
                                        }
					
					break;
				}
			}
		}
	}

	$scope.refine_search_cat = function(cat){
            	var new_url = '';
		
                var s_ar = '';
            angular.forEach($scope.cats, function (i) {

                if (!!i.selected) {

                    s_ar += i.url + ",";
                }

            });
                        
            var new_cat = s_ar.slice(0, -1);
                
                var exsis_url = window.location.href;
                
                exsis_url = $scope.remove_exis_param(exsis_url,'cat');
                var count = $scope.get_urlparams_count(exsis_url);
		if(parseInt(count) == 0){
                     new_url = exsis_url + "?cat="+cat.url;
                    
                }else{
                    new_url = exsis_url + "&cat="+cat.url;
                    
                }
                 $scope.browseUrl = new_url;
		
		return new_url;
	}
        
        $scope.get_urlparams_count = function (newurl){
            var str = newurl;
            var matches = str.match(/[a-z\d]+=[a-z\d]+/gi);
            return  matches ? matches.length : 0;
           
        }
        
        $scope.remove_exis_param = function (url , parameter){
            //prefer to use l.search if you have a location/link object
            var urlparts = url.split('?');
            if (urlparts.length >= 2) {

                var prefix = encodeURIComponent(parameter) + '=';
                var pars = urlparts[1].split(/[&;]/g);

                //reverse iteration as may be destructive
                for (var i = pars.length; i-- > 0; ) {
                    //idiom for string.startsWith
                    if (pars[i].lastIndexOf(prefix, 0) !== -1) {
                        pars.splice(i, 1);
                    }
                }

                url = urlparts[0] + (pars.length > 0 ? '?' + pars.join('&') : "");
                return url;
            } else {
                return url;
            }
        }

	$scope.refine_search = function(k, v, doNotLoadPage, no_multi, check_same){
		check_same = typeof check_same == 'undefined' ? false : check_same;
		
		var searchParams = panel.getParams();
		
		if($scope.params){
			
			for(var i in $scope.params){
				
				searchParams[i] = $scope.params[i];
			}
		}
		
		var vals = $scope.getValues(k);
            
		if(k != "" && v != ""){
			
			if(no_multi){
				
				if(typeof searchParams[k] != 'undefined'){
					
					if(v == searchParams[k] && check_same){
						
						delete searchParams[k];
					}
					else{
						
						searchParams[k] = v;
					}
				}
				else{
					
					searchParams[k] = v;
				}
			}
			else{
				
				vals.indexOf(v) > -1 ? vals.splice(vals.indexOf(v),1) : vals.push(v);
				
				var value = vals.join();
				
				if(value != ""){
					
					searchParams[k] = value;
				}
				else{
					
					delete searchParams[k];
				}
			}
		}
		
		var search_url = location.pathname+'?';
		
		var first = true;
		
		for(var i in searchParams){
			
			if(i != "page"){
				
				if(first){
					
					search_url = search_url+i+"="+searchParams[i];
					
					first = false;
				}
				else{
					
					search_url = search_url+"&"+i+"="+searchParams[i];
				}
			}
		}
		
		if(!doNotLoadPage){location.href = search_url;return;}
		
		$scope.params = searchParams;
		
		$scope.browseUrl = search_url;
		
		return search_url;
	}
	
	$scope.browse = function(cat){location.href = $scope.browseUrl;}
	
	$scope.phone = function(num){
		
		return panel.phone(num);
	}
	
	
	$scope.member_search = function(keyCode){
		                
		if(keyCode == 13){
			
			location.href= '/search?t=user&q='+$scope.query;
		}
                
		if(keyCode == 'is_mouse'){
			
			location.href= '/search?t=user&q='+$scope.query;
		}
	}
	
	
	$scope.get_address = function(){
		
		return panel.getParam('l');
	}
	
}]);


/* NG-SERVICE */

var ascoreServices = angular.module('ascoreServices', []);

ascoreServices.factory('panel', ['$http','$window', function($http,$window){

	var functions = {
	
		globals:{
			pageTitle:'Reviews of Dentists,Doctors, Lawyers, Renovation and Home Improvement Contractors, Fine Dining Restaurants - VeraScore',
			css:[],
			script:[],
			lookupData:{}
		},
		
		register_session_callback: function(callback){functions.globals.session_callback = callback;},
		
		data: function(element){
			
			var e = element ? element : 'data';
			
			var content = $(e).html();
			
			if(content) return JSON.parse(content);
			
			else return vsDATA;
		},
		
		bookmark: function(){
			
			if(window.sidebar && window.sidebar.addPanel){ /* Mozilla Firefox Bookmark */
				window.sidebar.addPanel(document.title,window.location.href,'');
            }
			else if(window.external && ('AddFavorite' in window.external)){ /* IE Favorite */
				window.external.AddFavorite(location.href,document.title); 
            }
			else if(window.opera && window.print){ /* Opera Hotlist */
				this.title=document.title; return true;
            }
			else{ /* webkit - safari/chrome */
				alert('Press ' + (navigator.userAgent.toLowerCase().indexOf('mac') != - 1 ? 'Command/Cmd' : 'CTRL') + ' + D to bookmark this page.');
            }
		},
		
		share: function(method, object){
			  
			
		},
		
		unhide: function(item){$(document).ready(function(){$(item).removeClass('hide');});},
		
		expose: function(item){$(document).ready(function(){$(item).show();});},
		
		prompt_review_type: function(id, name){
			
			$("#review-type-prompt").show();
			
			$("#write-a-text-review").attr('href', '/review/write?stage=2&type=text&id='+id+'&name='+name);
			
			$("#write-a-video-review").attr('href', '/review/write?stage=2&type=video&id='+id+'&name='+name);
		},
		
		alert: function(data){
		
			if(typeof data == 'object'){
			
				data = angular.toJson(data);
			}
		
			$('<pre></pre>').appendTo('body')
			.html('<div style="min-width:200px;margin-left:auto;margin-right:auto;white-space:normal;"><h6 style="font-size:15px;">'+data+'</h6></div>')
			.dialog({
				modal: true, title: 'message', zIndex: 10000, autoOpen: true,
				width: 'auto', resizable: false,
				buttons: {
					OK: function(){$(this).dialog("close");},
				},
			  
				close: function (event, ui) {
					$(this).remove();
				}
			});
		},
		
		confirm: function(question, yesCallback){
		
			$('<div></div>').appendTo('body')
			  .html('<div><h6>'+question+'</h6></div>')
			  .dialog({
				  modal: true, title: 'message', zIndex: 10000, autoOpen: true,
				  width: 'auto', resizable: false,
				  buttons: {
				  
					  Yes: function(){ (yesCallback)(); $(this).dialog("close");},
					  
					  No: function (){ $(this).dialog("close");}
				  },
				  close: function (event, ui) {
				  
					  $(this).remove();
				  }
			});
		},
		
		setPageTitle: function(title){
		
			this.globals['pageTitle'] = title;
		},
		
		getPageTitle: function(){
		
			return this.globals['pageTitle'];
		},
		
		setCssStyle: function(css){
		
			this.globals['css'] = css;
		},
		
		getCssStyle: function(){
		
			return this.globals['css'];
		},
		
		setScriptSrc: function(script){
		
			this.globals['script'] = script;
		},
		
		getScriptSrc: function(){
		
			return this.globals['script'];
		},
		
		get_url: function(){
			
			var n = location.href.indexOf('&fb_action_ids');
			
			if(n > 0){
				
				return location.href.substring(0, n);
			}
			
			return location.href;
		},
		
		get_website: function(){
			
			return location.host;
		},
		
		getHashedUrl: function(){
			
			var data = location.hash.substring(2).split('/');
			
			return data;
		},
		
		limit: function(str, limit){
	
			if(str){return str.substr(0, limit);}
			
			return str;
		},
		
		paragraph: function(str){
		
			return str.replace(/(?:\r\n|\r|\n)/g, '<br>');
		},
		
		postal_code: function(code){
			
			if(typeof code != 'undefined' && code.length == 6){
				
				return code.substr(0, 3) + ' ' + code.substr(3);
			}
			
			return code;
		},
		
		weekday: function(i){
			
			var weekdays = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];
			
			return weekdays[i];
		},
		
		phone: function(tel){
			
			if(typeof tel != 'undefined'){
				
				if(tel.length == 11){
					
					return '+'+tel.substr(0, 1)+ '(' + tel.substr(1, 3) + ') ' + tel.substr(4, 3) + '-' + tel.substr(7,4);
				}
				else if(tel.length == 10){
					
					return '(' + tel.substr(0, 3) + ') ' + tel.substr(3, 3) + '-' + tel.substr(6,4);
				}
				else if(tel.length == 7){
					
					return tel.substr(0, 3) + '-' + tel.substr(3,4);
				}
			}
		},
		
		getParams: function(){
		
			var sPageURL = window.location.search.substring(1);
			
			var sURLVariables = sPageURL.split('&');
			
			var params = {};
			
			for (var i = 0; i < sURLVariables.length; i++){
				
				var sParameterName = sURLVariables[i].split('=');
				
				if(sParameterName[0].length>0){ params[sParameterName[0]] = decodeURIComponent(sParameterName[1].replace(/\+/g, " "))};
			}
			
			return params;
		},
		
		getParam: function(name){
			name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
			
			var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
			
			results = regex.exec(location.search);
			
			var answer = results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
                        
			if(answer == ""){
				
				results = regex.exec(this.getHashedUrl());
				
				answer = results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
			}
			
			return answer;
		},
		
		loading: function(bool){
			
			if(bool){
				
				$('body').append('<div id="loading-gif"><img src="'+panel.__staticserv()+'/public/images/assets/loader.gif"></div>');
			}
			else{
				
				$("#loading-gif").remove();
			}
		},
		
		base64:{
			
			_keyStr: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",
			
			encode: function(input){
				
				var output = "";
				var chr1, chr2, chr3, enc1, enc2, enc3, enc4;
				var i = 0;

				input = functions.base64._utf8_encode(input);

				while (i < input.length){

					chr1 = input.charCodeAt(i++);
					chr2 = input.charCodeAt(i++);
					chr3 = input.charCodeAt(i++);

					enc1 = chr1 >> 2;
					enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
					enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
					enc4 = chr3 & 63;

					if (isNaN(chr2)){
						
						enc3 = enc4 = 64;
					}
					else if (isNaN(chr3)){
						enc4 = 64;
					}

					output = output + this._keyStr.charAt(enc1) + this._keyStr.charAt(enc2) + this._keyStr.charAt(enc3) + this._keyStr.charAt(enc4);

				}

				return output;
			},

			decode: function(input){
				
				var output = "";
				var chr1, chr2, chr3;
				var enc1, enc2, enc3, enc4;
				var i = 0;

				input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");

				while (i < input.length){

					enc1 = this._keyStr.indexOf(input.charAt(i++));
					enc2 = this._keyStr.indexOf(input.charAt(i++));
					enc3 = this._keyStr.indexOf(input.charAt(i++));
					enc4 = this._keyStr.indexOf(input.charAt(i++));

					chr1 = (enc1 << 2) | (enc2 >> 4);
					chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
					chr3 = ((enc3 & 3) << 6) | enc4;

					output = output + String.fromCharCode(chr1);

					if (enc3 != 64) {
					output = output + String.fromCharCode(chr2);
					}
					if (enc4 != 64) {
					output = output + String.fromCharCode(chr3);
					}
				}

				output = functions.base64._utf8_decode(output);

				return output;
			},

			_utf8_encode: function(string){
				
				string = string.replace(/\r\n/g, "\n");
				var utftext = "";

				for (var n = 0; n < string.length; n++) {

					var c = string.charCodeAt(n);

					if (c < 128) {
						utftext += String.fromCharCode(c);
					}
					else if ((c > 127) && (c < 2048)) {
						utftext += String.fromCharCode((c >> 6) | 192);
						utftext += String.fromCharCode((c & 63) | 128);
					}
					else {
						utftext += String.fromCharCode((c >> 12) | 224);
						utftext += String.fromCharCode(((c >> 6) & 63) | 128);
						utftext += String.fromCharCode((c & 63) | 128);
					}

				}

				return utftext;
			},

			_utf8_decode: function(utftext){
				
				var string = "";
				var i = 0;
				var c = c1 = c2 = 0;

				while (i < utftext.length) {

					c = utftext.charCodeAt(i);

					if (c < 128) {
						string += String.fromCharCode(c);
						i++;
					}
					else if ((c > 191) && (c < 224)) {
						c2 = utftext.charCodeAt(i + 1);
						string += String.fromCharCode(((c & 31) << 6) | (c2 & 63));
						i += 2;
					}
					else {
						c2 = utftext.charCodeAt(i + 1);
						c3 = utftext.charCodeAt(i + 2);
						string += String.fromCharCode(((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
						i += 3;
					}

				}

				return string;
			}
		},
	
		fetch: function(url, callback, cacheResult, animate){
			
			if(animate){functions.loading(true);}
			
			if(!cacheResult){
				
				$http({
					method  : 'GET',
					url     : url,
					headers : {
						'cache-control': 'public,max-age=7200',
						'Expires' : '7200'
					}
				})
				.success(function(response){
				
					if (callback){(callback)(response)};
				})
			}
			else{
				
				$http({
					method  : 'GET',
					url     : url
				})
				.success(function(response){
				
					if (callback){(callback)(response)};
				})
			}
			
		},
		
		url_string: function(str){
			
			if(str){
				return str.replace(/,/g, " ").replace(/\s+/g, " ").replace(/\s/g, "-").replace(/(\&amp;|\&)/g,'and').replace(/\'/g,'').toLowerCase();
			}
		},
		
		community_keywords: function(str){
		
			var tags = str.split(',');
			
			var html = '';
			
			for(i in tags){
			
				html = html+ '<a href="/community/search/'+functions.url_string(tags[i])+'" target="_blank" class="keyword">'+tags[i]+'</a>';
			}
			
			return html;
		},
		
		loading: function(loader){loader ? $('body').append("<div id='loading-gif'></div>") : $('#loading-gif').remove();},
		
		post: function(url, formData, callback, cache, show_loader){
			
			var headers = {'Content-Type': 'application/x-www-form-urlencoded'};
			
			if(cache){
				
				headers['cache-control'] = 'public,max-age=7200';
				headers['Expires'] = '7200';
			}
			
			functions.loading(show_loader);
			
			$http({
				method  : 'POST',
				url     : url,
				data    : $.param(formData),
				headers : headers	
			})
			.success(function(response){
			
				if (callback){(callback)(response)};
				
				functions.loading(false);
			})
			.error(function(){
				
			})
		},
		
		upload: function(url, formData, callback, show_loader){
		
			functions.loading(show_loader);
			
			$http.post(
				url, 
				formData,
				{
					transformRequest: angular.identity,
					headers			: {'Content-Type': undefined}
				}	
			)
			.success(function(response){
			
				if (callback){(callback)(response)};
				
				functions.loading(false);
			})
			.error(function(){
				
			})
		},
		
		serializeObject: function(serializedArray){
		
			var o = {};
			
			var a = serializedArray;
			
			$.each(a, function(){
			
				if (o[this.name] !== undefined){
				
					if (!o[this.name].push){
					
						o[this.name] = [o[this.name]];
						
					}
					
					o[this.name].push(this.value || '');
					
				}else{
				
					o[this.name] = this.value || '';
				}
			});
			
			return o;
		},
		
		/* WOOT WOOT ... */
		/*
		 These functions were added because there was no better way to make them global - so the entire service was made global because of these
		 since their availability should be everywhere on the website
		*/
		send_kudos: function(to, reviewer_id, review_id, callback){
		
			$('<div id="popup-dialog"></div>').appendTo('body')
			.html(
				'<div><form id="send_kudos"><p>To: '+to+'</p>\
				<input type=hidden name="receipient_id" value="'+reviewer_id+'">\
				<input type=hidden name="type" value="kudos">\
				<input type=hidden name="review_id" value="'+review_id+'">\
				<textarea style="width:450px;height:100px;background-image: url(bg.jpg);" placeholder="Kudo\'s to '+to+'" name="message"></textarea></form></div>'
			)
			 .dialog({
				  modal: true, title: 'Send Kudo\'s', zIndex: 10000, autoOpen: true,
				  width: 'auto', resizable: false,
				  buttons:{
				  
					"Send Kudo's": function(){
							
							var data = functions.serializeObject($("#send_kudos").serializeArray());
							
							functions.post('/incoming', data, function(response){
								
								if(response.Request == 'OK'){
								
									$('#popup-dialog').dialog("close");
									
									functions.alert('Thanks, Kudos send!');
									
									if(callback){(callback)(response);}
								}
							});
						},
					  
					  "Cancel": function (){ $(this).dialog("close");}
				  },
				  close: function (event, ui){
				  
					  $(this).remove();
				  }
			});
		},
		
		send_message: function(to, receipient_id, review_id, callback){
		
			$('<div id="popup-dialog"></div>').appendTo('body')
			.html(
				'<div><form id="send_message"><p>To: '+to+'</p>\
				<input type=hidden name="receipient_id" value="'+receipient_id+'">\
				<input type=hidden name="type" value="mail">\
				<p><input type=text name="subject" style="width:450px;" placeholder="Subject!" /></p>\
				<p><textarea name="message" style="width:450px;height:100px" placeholder="What\'s your message?"></textarea></form></p></div>'
			)
			 .dialog({
				  modal: true, title: 'Send Message', zIndex: 10000, autoOpen: true,
				  width: 'auto', resizable: false,
				  buttons:{
				  
					"Send Message": function(){
							
							var data = functions.serializeObject($("#send_message").serializeArray());
							
							functions.post('/incoming', data, function(response){
								
								if((response.Request) == 'OK'){
								
									$('#popup-dialog').dialog("close");
									
									functions.alert('Message send!');
									
									if(callback){(callback)(response);}
								}
							});
						},
					  
					  "Cancel": function (){ $(this).dialog("close");}
				  },
				  close: function (event, ui){
				  
					  $(this).remove();
				  }
			});
		},
		
		add_photos: function(business_id, callback){
		
			functions.fetch('/access/session', function(response){
				
				if(response.Request == 'OK'){
					
					$('<input type="file" name="files[]" multiple id="file-picker"/>')
					.appendTo('body')
					.css({'display':'none'})
					.click()
					.on('change', function(){
					
						var files = this.files;
						
						var form = new FormData();
						
						for(var i in files){
						
							form.append('images[]', files[i]);
						}
						
						form.append('data', JSON.stringify({'business_id':business_id}));
						
						functions.upload('/incoming/upload', form, function(response){
						
							if(response.Request == 'OK'){
							
								functions.alert("You images have been uploaded successfully");
							}
						});
					});
				}
				else{
					
					functions.alert("You must be logged in to upload images!!!");
				}
			});
		},
		
		follow: function(user_id, callback){
		
			var data = {'user_id':user_id,'type':'follow'};
							
			functions.post('/incoming', data, function(response){
				
				if((response.Request) == 'OK'){
				
					functions.alert('Following successful!');
					
					if(callback){(callback)(response);}
				}
			});
		},
		
		report_problem: function(title, user_id, content_id, scope_id, callback){
		
			functions.fetch('/house/data?query=report_labels', function(report_labels){
			
				var select_label = '<select name="report_label_id" style="width:450px;">';
			
				for(i in report_labels){
				
					select_label = select_label + '<option value="'+report_labels[i]['id']+'">'+report_labels[i]['label']+'</option>';	
				}
				
				select_label = select_label + '</select>';
				
				$('<div id="popup-dialog"></div>').appendTo('body')
				.html(
					'<div><form id="report_problem">\
					<p style="width:450px;">Problem With: <br><strong>'+title+'</strong></p>\
					<input type=hidden name="type" value="report">\
					<input type=hidden name="user_id" value="'+user_id+'">\
					<input type=hidden name="content_id" value="'+content_id+'">\
					<input type=hidden name="scope_id" value="'+scope_id+'">Please identify problem:<br>\
					'+select_label+'<br><textarea style="width:450px;height:100px" name="message" placeholder="Briefly describe problem"></textarea></form></div>'
				)
				 .dialog({
					  modal: true, title: 'Report Problem', zIndex: 10000, autoOpen: true,
					  width: 'auto', resizable: false,
					  buttons:{
					  
						"Report Problem": function(){
								
								var data = functions.serializeObject($("#report_problem").serializeArray());
								
								functions.post('/incoming', data, function(response){
									
									if(response.Request == 'OK'){
									
										$('#popup-dialog').dialog("close");
										
										functions.alert('Thank you for your report. Our team will look at it shorty!');
										
										if(callback){(callback)(response);}
									}
								});
							},
						  
						  "Cancel": function (){ $(this).dialog("close");}
					  },
					  close: function (event, ui){
					  
						  $(this).remove();
					  }
				});
			});
		},
		
		getPlace: function(x){
			
			functions.getCookie('searchPlace');
		},
		
		setPlace: function(x){
			
			functions.setCookie('searchPlace', x);
		},
		
		setCookie: function(cname, cvalue){
			var exdays = 2;
			var d = new Date();
			d.setTime(d.getTime() + (exdays*24*60*60*1000));
			var expires = "expires="+d.toUTCString();
			document.cookie = cname + "=" + cvalue + "; " + expires;
		},
		
		getCookie: function(cname){
			var name = cname + "=";
			var ca = document.cookie.split(';');
			for(var i=0; i<ca.length; i++){
				var c = ca[i];
				while (c.charAt(0)==' ') c = c.substring(1);
				if (c.indexOf(name) == 0) return c.substring(name.length,c.length).replace(/%22/g,'');
			}
			return "";
					
		},
		
		getPlace: function(){
			
			var x = functions.getCookie('searchPlace');
			
			return (x !== "") ? x : 'Toronto';
		},
		
		getLocality: function(){
			
			var x = functions.getCookie('searchLocality');
			
			return (x !== "") ? x : 'Toronto';
		},
		
		getLocale: function(){
			
			var x = functions.getCookie('searchLocale');
			
			return (x !== "") ? x : 'ON';
		},
		
		defined: function(item){
			
			if(typeof item == 'undefined'){ return false;}
			
			return true;
		},
		
		
            tipBusiness: function(id, event){
			
			var elem = $("#tip-for-biz-"+id);
			
			if(!elem.hasClass('hide')){
				
				elem.addClass('hide');
			}
			else{
				
				elem.removeClass('hide');
			}
			
		},
		
		tipUserScore: function(id){
		
			var overlay = $("#tip-overlay-"+id);
			
			if(!overlay.hasClass('hide')){
				
				overlay.addClass('hide');
			}
			else{
				
				overlay.removeClass('hide');
			}
		},
		
		tipUserInfo: function(id){
		
			var elem = $("#tip-for-user-"+id);
			
			if(!elem.hasClass('hide')){
				
				elem.addClass('hide');
			}
			else{
				
				elem.removeClass('hide');
			}
		},
		
		location: function(url){
			
			location.href = url;
		},
		
		ERROR: {
			
			is_valid: function(response){
				
				if(typeof response.Request  != 'undefined' && response.type !== 'undefined'){ return true;}
				
				return false;
			},
			
			login_required: function(response){
				
				if(functions.ERROR.is_valid(response)){
					
					if(response.type == 'login_required'){
						
						return true;
					}
				}
				
				return false;
			}
		},
		
		inpage_callback: [],
		
		inpage_login: function(hide, callback){
			
			var element = $("#inline-access-form");
			
			if(!hide){
				
				element.show();
				
				if($('body').attr('id') == 'handheld'){$('html, body').scrollTop(0);}
			}
			else{element.hide();}
			
			if(typeof callback != 'undefined'){functions.inpage_callback.push(callback);}
			
			if(functions.globals.session_callback){(functions.globals.session_callback)()};
		},
		
		continueIfAccess: function(callback){
			
			functions.fetch('/house/alive', function(response){
			
				if(response.Request == 'OK'){
					
					(callback)();
				}
				else if(functions.ERROR.login_required(response)){
							
					functions.inpage_login(false, function(){
						
						(callback)();
					});
				}
			});
		},
		
		gapiInit: function(callback){
			
			$window.init_Gapi = function(){
				
				if(typeof $window.gapi != 'undefined'){
					
					functions.gapi = $window.gapi;
					
					(callback)();
				}
			}
			
			var s = document.createElement("script");
		
			s.src = "https://apis.google.com/js/client.js?onload=init_Gapi"; $("html").append(s);
		},
		
		embed_video: function(review, isMobile, width, height, suffix){
			
			if(!width){width = isMobile ? "100%" : "500";}
						
			if(!height){height = isMobile ? $('body').attr('tablet') == 'tablet' ? '300' : "100%" : "275";}
		
			if(typeof review == 'object' && review.video != null){
				
				$(document).ready(function(){
					
					var id = $.parseJSON(review.video).id;
					
					var elem = !suffix ? $("#video-"+review.id) : $("#"+suffix+review.id);
					
					if(typeof id !="undefined" && elem.find('iframe').length == 0){
						
						var y = document.createElement('iframe'); y.src = "https://www.youtube.com/embed/"+id.trim(); y.width = width; y.height=height;
						
						var a = document.createAttribute("allowfullscreen");
						
						y.setAttributeNode(a);
						
						elem.append(y);
					}
				});
			}
		},
		
		sizeOf: function(obj){
			
			var count = 0;
    
			if (typeof obj == "object"){

				if (Object.keys){
					
					count = Object.keys(obj).length;
				}
				else if (window._){
					
					count = _.keys(obj).length;
				}
				else if (window.$){
					
					count = $.map(obj, function() { return 1; }).length;
				}
				else{
				
					for (var key in obj){
						
						if (obj.hasOwnProperty(key)){count++;}
					}
				}

			}

			return count;
		},
		
		__staticserv: function(){return 'https://static.ascore.ca';}
	}
	
	return functions;
}]);


/* NG-FILTER */

ascoreFilters = angular.module('ascoreFilters', []);

ascoreFilters.filter('hyphenate', [function(){

	return function(input){
	
		return input.trim().replace(/\s/g, '-');
	};
}]);

ascoreFilters.filter('range', [function(){

  return function(input, total){
  
    total = parseInt(total);
	
    for (var i=0; i<total; i++){
	
      input.push(i);
	  
	}
    return input;
  };
}]);


/* NG-DIRECTIVE */

var ascoreDirectives = angular.module('ascoreDirectives',[]);

ascoreDirectives.directive('reviewQuestions', [function(){
	
	return function(scope, element, attrs){
	
		for(q in scope.questions){
			
			var question = scope.questions[q];
			
			if(question.id == attrs['question']){
			
				scope.set_rating(question, scope.review.answers[question.id], element);
			}
		}
	}

}]);

ascoreDirectives.directive('searchSuggestion', [function(){
	
	return function(scope, element, attrs){
	
		$(document).ready(function(){
			
			$(element).autocomplete({
				
				source: function(request, response){
					$.ajax({
						url: '/search',
						data: {
							't':'suggestion',
							'q':request.term,
							'j':1,
							l:scope.search.place.label
						}
					}).success(function(data){
						
						var terms = {};
						
						var list = $.parseJSON(data);
						
						for(obj in list)
						{
							terms[list[obj].id] = list[obj].term;
						}
						
						response($.map(list, function(info){
							
							var x = {'value' : info.term,'label' : info.term}
							
							for(var i in info) x[i] = info[i];
							
							return x;
						}));
					});
				},
				
				select: function(event, ui){
					
					scope.query = ui.item.label;
				}
			}).autocomplete( "instance" )._renderItem = function( ul, item ){
			
			/*****************************************************
			* DISABLED TEMPORALIRY FOR REASONS NOT SPECIFIED
			  return $( "<li>" ).append( "<a style='width:100%;display:block' href='/biz/"+item.url+"/"+item.business_id+"'>" + item.term + 
										"<small class='blue hide'>" + item.category + " > " +
										item.city +", " + item.province_code + "</small></br>" +
										"</a>" ).appendTo( ul );
			*****************************************************/
			var parent = (item.parent_url != '') ? item.parent_url + "/" : '';
                                
			 if(item.parent == "")
                        {
                            return $( "<li>" ).append( "<a style='width:100%;display:block' href='javascript:void(0);'><b>" + item.term + "</b></a>" ).appendTo( ul );

                        }else{
                            return $( "<li>" ).append( "<a style='width:100%;display:block' href='javascript:void(0);'>" + item.parent+" <i class='glyphicon glyphicon-hand-right' style='color:#0084B4;'></i><b> "+item.term+"</b></a>" ).appendTo( ul );
                        }	
                   
                   }                        
                       
		});
	}
	
}]);

ascoreDirectives.directive('reviewCitySearch', [function(){

	return function(scope, element, attrs){
	
		$(element).autocomplete({
		
			css: { background:'red'},
			
			source: function(request, response){
				$.ajax({
					url: '/search',
					data: {
						't':'city',
						'q':request.term,
						'j':1,
						'pid':scope.get_province()
					}
				}).success(function(data){
					
					var cities = {};
					
					var list = $.parseJSON(data);
					
					for(obj in list)
					{
						cities[list[obj].id] = list[obj].name;
					}
					
					response($.map(list, function(info){
						
						var x = {
						'value' : info.name,
						'label' : info.name,
						'city_id':info.id
						}
						
						return x;
					}));
				});
			},
			
			select: function(event, ui){
				
				scope.review.location.city_id = ui.item.city_id;
				
				scope.review.location.city = ui.item.label;
			}
		});
	}
}]);


ascoreDirectives.directive('businessSearch', [function(){

	return function(scope, element, attrs){
	
		$(element).autocomplete({
		
			css: { background:'red'},
			
			source: function(request, response){
				$.ajax({
					url: '/search',
					data: {
						't':'business',
						'q':request.term,
						'j':1
					}
				}).success(function(data){
					
					var businesses = {};
					
					var search_r = $.parseJSON(data);
					
					var list = scope.businesses = search_r[0];
					
					for(obj in list)
					{
						businesses[list[obj].id] = list[obj].name;
					}
					
					response($.map(list, function(info){
						
						var x = {
						'value' : info.name,
						'label' : info.name,
						'business_id':info.business_id
						}
						
						for(i in info){
						
							x[i] = info[i];
						}
						
						return x;
					}));
				});
			},
			
			select: function(event, ui){
			
				scope.search = {};
				
				scope.business_search = ui.item.name;
				
				scope.search.business = ui.item;
			}
		});
	}
}]);

ascoreDirectives.directive('googleMaps', ['panel', function(panel){

	return function(scope, element, attrs){
	
		var mapCanvas = document.getElementById('map-canvas');
		
		if(attrs.index){var pageItem = parseInt(attrs.index);}
		
		var mapOptions = {
			
			zoom: 11,
		 
			mapTypeId: google.maps.MapTypeId.ROADMAP
		}
		
		var address = scope.get_address();
                
		
		if(address.length == 0){
			
			mapOptions.center = new google.maps.LatLng(44.5403, -78.5463);
		}
		
		var bindInfoWindow = function(marker, map, infowindow, html){
		
			google.maps.event.addListener(marker, 'click', function(){
				
				infowindow.setZIndex(99999);
				
				infowindow.setContent(html); 
				
				infowindow.open(map, marker); 
			}); 
			/*
			google.maps.event.addListener(marker, 'mouseout', function(){
				
				infowindow.close(map, marker); 
			}); */
		}

		var map = new google.maps.Map(mapCanvas, mapOptions);
		
		if(pageItem){
			
			var bounds = new google.maps.LatLngBounds();
		}
		
		var is_business_page = false;
		
		var mark_address = function(address, index){
			
			var image = !is_business_page ? panel.__staticserv()+'/public/images/assets/location_pin.png' : panel.__staticserv()+'/public/images/assets/location_pin.png';
			
			var imageObj = new Image();
			
			imageObj.src = image;
			
			var geocoder = new google.maps.Geocoder();
                        
			imageObj.onload = function(){
				                    					
				var canvas = document.createElement('canvas');
				
				canvas.width = 33;
				
				canvas.height = 40;
				
				var context = canvas.getContext("2d");
				
				context.drawImage(imageObj, 0, 0);
				
				context.font = "12px ProximaNova-Regular";
				
				context.fillStyle = 'white';
				
				if(pageItem){
					
					var x = 12, y = 20, j = parseInt(index),i = (pageItem == 1) ? j+1 : ((pageItem*10)-10)+1+j;
					
					context.fillText(i, x, y);
				}

				geocoder.geocode({'address': address}, function(results, status){
				
					if(status == google.maps.GeocoderStatus.OK){
						var marker = new google.maps.Marker({
							position: results[0].geometry.location,
							map: map
						});

						var infowindow =  new google.maps.InfoWindow({content: ''});

						if(index){
							
							bindInfoWindow(marker, map, infowindow, scope.get_info_window(index));

							bounds.extend(marker.position);map.fitBounds(bounds);
						}
						else{

							map.setCenter(results[0].geometry.location);

							if(scope.get_info_window){

								bindInfoWindow(marker, map, infowindow, scope.get_info_window());
							}
						}
					}
					else{
						console.log('Geocode was not successful for the following reason: ' + status);
					}
				});
			}
		}
		
		$(document).ready(function(){
			
			if(address.length > 0){is_business_page=true;mark_address(address);}
			
			$(".business-address").each(function(){
				
				address =  $(this).html().replace(/(<([^>\-\[\]]+)>|\n+|\t+|\|((<!--).*(-->))" class=\"ng-scope\">)/ig,"");
				
				if(address.length > 0){mark_address(address, $(this).attr('index'));}
			});
		});
	}

}]);

ascoreDirectives.directive('citySearch', [function(){

	return function(scope, element, attrs){
		
		$(element).autocomplete({
		
			css: { background:'red',position:'relative','z-index':10000000},
			
			source: function(request, response){
				$.ajax({
					url: '/search',
					data: {
						't':'city',
						'q':request.term,
						'j':1,
						'pid':scope.get_search ? scope.get_search('province') : 0
					}
				}).success(function(data){
					
					var cities = {};
					
					var list = $.parseJSON(data);
					
					for(obj in list)
					{
						cities[list[obj].id] = list[obj].name;
					}
					
					response($.map(list, function(info){
						
						var x = {
						'value' : info.name,
						'label' : info.name+', '+info.province,
						'city_id': info.city_id,
						'province': info.province
						}
						
						for(i in info){
						
							x[i] = info[i];
						}
						
						return x;
					}));
				});
			},
			
			select: function(event, ui){
				
				if(!scope.search){
					
					scope.search = {};
				}
				
				scope.search.city = ui.item;
			}
		});
	}
}]);

ascoreDirectives.directive('categorySearch', [function(){

	return function(scope, element, attrs){
		
		$(element).autocomplete({
		
			css: { background:'red',position:'relative','z-index':10000000},
			
			source: function(request, response){
				$.ajax({
					url: '/search',
					data: {
						't':'qcategory',
						'q':request.term,
						'j':1
					}
				}).success(function(data){
					var categories = {};
					
					var list = $.parseJSON(data);
					for(obj in list)
					{
						categories[obj] = list[obj];
					}
					response($.map(list, function(info){
						
						var x = {
						'value' : info,
						'label' : info
						}
						
						for(i in info){
						
							x[i] = info[i];
						}
						return x;
					}));
				});
			},
			
			select: function(event, ui){
                scope.editing_word  = ui.item.value;
			}
		});
	}
}]);

ascoreDirectives.directive('locationSearch', ['panel', function(panel){

	return function(scope, element, attrs){
		
		$(element).keyup(function(){
			
			if(!$(this).hasClass('typing')){
				
				$(this).addClass('typing');
			}
		});
		
		$(document).ready(function(){
			
			$(element).autocomplete({
			
				css: { background:'red',position:'relative','z-index':10000000},
				
				source: function(request, response){
					$.ajax({
						url: '/search',
						data: {
							't':'location',
							'q':request.term,
							'j':1
						}
					}).success(function(data){
						
						var places = {};
						
						var list = $.parseJSON(data);
						
						for(obj in list)
						{
							places[list[obj].id] = list[obj].name;
						}
						
						response($.map(list, function(info){
							
							return info;
						}));
					});
				},
				
				select: function(event, ui){
					
					if(!scope.search){
						
						scope.search = {};
					}
					
					scope.search.place = ui.item;
					
					panel.setPlace(scope.search.place);
				}
			});
		});
	}
}]);

ascoreDirectives.directive('elastic', [
    '$timeout',
    function($timeout) {
      return {
        restrict: 'A',
        link: function(scope, element) {
          var resize = function() {
            return element[0].style.height = "" + element[0].scrollHeight + "px";
          };
          element.on("blur keyup change", resize);
          $timeout(resize, 0);
        }
      };
    }
]);



ascoreDirectives.directive('businessRating', ['panel', function(panel){
	
	return{
		
        restrict: 'A',
		
        link: function(scope, element, attrs){
			
			var img = '<img src="'+panel.__staticserv()+'/public/images/ratings-icon-circle-version.png" />';
			
			$(element).append(img);
		}
	}
}]);

ascoreDirectives.directive('ngGallery', ['panel', '$compile', function(panel, $compile){
	
	return{
		
        restrict: 'E',
		
        link: function(scope, element, attrs){
			
			scope.current_gallery_index = 0;
			
			scope._thumbs_limit = attrs.thumbs_limit;
			
			scope._launch_class = attrs.launch;
			
			scope.gallery_set_image = function(){
				
				var image = document.getElementById("gallery-image-viewer");
				
				image.src = panel.__staticserv()+"/public/images/transparent.png";
				
				var downloadingImage = new Image();
				
				downloadingImage.onload = function(){image.src = this.src;};
                
				downloadingImage.onerror = function() {
                    image.src = "/public/images/image-not-found.png";
                }
                
				downloadingImage.src = panel.__staticserv()+'/images/'+scope._gallery_imgs[scope.current_gallery_index].image;
			}
			
			scope.gallery_next_image = function(){
				
				var i = scope.current_gallery_index + 1;
				
				if(i < scope._gallery_imgs.length){scope.current_gallery_index = i;scope.gallery_set_image();}
			}
			
			scope.gallery_prev_image = function(){
				
				var i = scope.current_gallery_index - 1;
				
				scope.current_gallery_index = i;
				
				scope.gallery_set_image();
			}
			
			scope.launch_gallery = function(gallery, index, callback){
				
				scope._gallery_imgs = gallery;
				
				if(typeof index != 'undefined'){scope.current_gallery_index = index;scope.gallery_set_image();}
				
				$('.my-gallery').show();
                                
                                $('.my-gallery').css({
                                    'background-color': '#000000'
                                });
				
				if(callback){scope[callback](function(response){
					
					scope._gallery_imgs = response;
				})};
			}
			
			scope.set_gallery_index = function(index){scope.current_gallery_index = index;scope.gallery_set_image();}
			
			panel.fetch('/house/popup?p=image-gallery', function(response){
				
				var html = $(response);
				
				$compile(html)(scope);
				
				$(element).append(html);
				
				$(document).ready(function(){$('.gallery-close').click(function(){$('.my-gallery').hide();});});
                                
                                /*
                                * on event override for gallery close
                                *   
                                */
                               $(".gallery-close").on("click", function () {
                                   $('.my-gallery').hide();
                               });
			});
		}
	}
}]);


ascoreDirectives.directive('instantActionRequest', ['panel', '$compile', function(panel, $compile){
	
	return{
		
        restrict: 'A',
		
        link: function(scope, element, attrs){
			
			var displayPopup = function(response){
				
				var html = $(response);
						
				$compile(html)(scope);
				
				$('body').append(html);
			}
			
			scope._cancel = function(){$('.inline-popup').hide();}
			
			var actions = {
				
				kudos: function(){
					
					if((typeof attrs.mine != 'undefined') && attrs.mine == 'true'){
						
						panel.alert("Cannot send yourself a kudo."); return;
						/*$(element).prop("disabled", true);*/
					}
					
					panel.fetch('/house/popup?p=send-kudos', function(response){
						
						scope.__kudos__ = {type:'kudos', receipient_id: attrs.uid, review_id:attrs.review, message:''};
						
						scope._send_kudos = function(){
							
							panel.post('/incoming', scope.__kudos__, function(response){
									
								if(response.Request == 'OK'){
									
									$(element).prop("disabled", true);
									
									scope._cancel();
									
									panel.alert('Thanks, Kudos send!');
								}
							});
						}
						
						displayPopup(response);
						
						if(attrs.callback){(scope[attrs.callback])(attrs.index);}
		
					}, true);
				},
				
				message: function(){
					
					if((typeof attrs.mine != 'undefined') && attrs.mine == 'true'){
						
						panel.alert("Cannot send yourself a message");return;
						/*$(element).prop("disabled", true);*/
					}
					
					panel.fetch('/house/popup?p=send-message', function(response){
						
						scope.__message__ = {type:'mail',scope:attrs.scope,receipient_id:attrs.uid,subject:'',message:''};
						
						if(attrs.review){scope.review_id = attrs.review;}
						
						if(attrs.rtype){scope.rtype = attrs.rtype;}
						
						scope._send_message = function(){
							
							scope._error_ = '';
							
							if(typeof scope.__message__.subject == 'undefined' || scope.__message__.subject.trim().length == 0){
								
								scope._error_ = 'Please enter a subject.'; return;
							}
							
							if(typeof scope.__message__.message == 'undefined' || scope.__message__.message.trim().length == 0){
								
								scope._error_ = 'Please enter a message'; return;
							}
							
							panel.post('/incoming', scope.__message__, function(response){
											
								if((response.Request) == 'OK'){
									
									scope._cancel();
									
									panel.alert('Your message was sent successfully. Thank you!');
								}
							});
						}
						
						displayPopup(response);
						
					}, true);
				},
                
                accept_friend : function(){
                    if (scope.disableLink == true){
                        return;
                    }
                    scope.$parent.showSpinner = true;
                    scope.$parent.disableLink = true;
                    var data = {type:'accept_friend_request', follower_id:attrs.uid, is_friend:1};
					
					panel.post('/incoming', data, function(response){
						
						if((response.Request) == 'OK'){
							
							if(!response.friends){panel.alert('Friend Request Accepted!');}
							
                            
                            scope.$parent.disableLink = false;
							
							if(attrs.callback){(scope[attrs.callback])();}
                            
                            scope.$parent.showSpinner = false;
						}
					});
                },
                
				friend: function(){
                    
                    if (scope.disableLink == true){
                        return;
                    }
                    scope.$parent.showSpinner = true;
                    scope.$parent.disableLink = true;
					if((typeof attrs.relation != 'undefined')){
				
						if(attrs.relation == 'friend'){
							
							panel.alert('You are friends with this person!');return;
							
							/*$(element).prop("disabled", true);*/
						}
						else if(attrs.relation == 'requested'){
							
							panel.alert('You already send a friend request to this person!');return;
							
							/*$(element).prop("disabled", true);$(element).find('span').html('Friend Requested');*/
						}
						else if(attrs.relation == 'revoked'){
							
							panel.alert('This user is not accepting requests at the moment!');return;
							
							/*$(element).prop("disabled", true);*/
						}
					}
			
					var data = {type:'friend', user_id:attrs.uid};
					
					panel.post('/incoming', data, function(response){
						
						if((response.Request) == 'OK'){
						
							$(element).prop("disabled", true);
							
							$(element).find('span').html('Friend Requested');
							
							if(!response.friends){panel.alert('Friend Request Send!');}
							
							var isFriend = !response.friends ? false: true;
                            
                            scope.$parent.disableLink = false;
							
							if(attrs.callback){(scope[attrs.callback])(attrs.index, isFriend);}
                            
                            scope.$parent.showSpinner = false;
						}
					});
				},
				
				unfriend: function(){
					
					panel.confirm("Are you sure you want to unfriend this user?", function(){
						scope.$parent.showSpinner = true;
						var data = {type:'unfriend', user_id:attrs.uid};
								
						panel.post('/incoming', data, function(response){
							
							if((response.Request) == 'OK'){
								
								if(attrs.callback){(scope[attrs.callback])(attrs.index);}
                                scope.$parent.showSpinner = false;
							}
						});
					});
				},
				
				block: function(){
					
					panel.confirm("You are about to block this user. Confirm!", function(){
						
						var data = {type:'block', user_id:attrs.uid};
								
						panel.post('/incoming', data, function(response){
							
							if((response.Request) == 'OK'){
								
								if(attrs.callback){(scope[attrs.callback])(attrs.index);}
							}
						});
					});
				},
				
				follow: function(){
					
					var data = {type:'follow', user_id:attrs.uid};
							
					panel.post('/incoming', data, function(response){
						
						if((response.Request) == 'OK'){
						
							$(element).prop("disabled", true);
							
							panel.alert('Following successful!');
							
							if(attrs.callback){(scope[attrs.callback])(attrs.index);}
						}
					});
				},
				
				unfollow: function(){
					
					panel.confirm("You are about to unfollow this user. Confirm!", function(){
						
						var data = {type:'unfollow', user_id:attrs.uid};
						
						panel.post('/incoming', data, function(response){
							
							if((response.Request) == 'OK'){
								
								if(attrs.callback){(scope[attrs.callback])(attrs.index);}
							}
						});
					});
				},
				
				report: function(){
					
					var reported = $(element).attr('reported');
					
					if(reported.length > 0 && reported!=0 && reported!="[]"){
						
						panel.alert("<span class='bold'>You already posted a report on this post.</span>\
									<br><br>\
									NOTE: It may take up to 24 hours for you request to be handled.");
						
						return;
					}
					
					var check = ['type','user_id','content_id','scope_id','report_label_id','message'];
					
					var callback = $(element).attr('callback');
					var clicked =  $(element).attr('clicked');
          if(clicked == 0 || clicked == undefined ) {
					var data = {
							type : 'report',
							title : $(element).attr('reporting'),
							content_id : $(element).attr('content_id'),
							scope_id : $(element).attr('scope_id')
						};
					$(element).attr('clicked',1);					
					panel.fetch('/house/data?query=report_labels', function(report_labels){
					
						var select_label = '<select id="report_label_id" class="form-control">';
					
						for(i in report_labels){
						
							select_label = select_label + '<option value="'+report_labels[i]['id']+'">'+report_labels[i]['label']+'</option>';	
						}
						
						select_label = select_label + '</select>';
						
						$('<div id="popup-dialog"></div>').appendTo('body')
						.html(
							'<div>\
								<form id="report_problem">\
									<p style="width:450px;">Problem With: <br><strong>'+data.title+'</strong></p>\
									Please identify problem:<br>\
									'+select_label+'<br>\
									<textarea class="form-control" style="height:100px" id="report_label_message" placeholder="Briefly describe problem"></textarea>\
								</form>\
							</div>'
						)
						 .dialog({
							  modal: true, title: 'Report Problem', zIndex: 10000, autoOpen: true,
							  width: 'auto', resizable: false,
							  buttons:{
							  
								"Report Problem": function(){
										
										delete data.title;
										
										data.report_label_id = $("#report_label_id").val();
										
										data.message = $("#report_label_message").val();
										
										if($("#report_label_message").val().trim().length> 0){
										panel.post('/incoming', data, function(response){
											
											if(response.Request == 'OK'){
											
												$('#popup-dialog').dialog("close");
												
												panel.alert('Thank you for your report. Our team will look at it shorty!');
												
												if(callback){(scope[callback])(response);}
											}
										});
										}else{
										panel.alert("Please write some description"); return;	
										}
									},
								  
								  "Cancel": function (){ $(this).dialog("close");}
							  },
							  
							  close: function (event, ui){
							    $(element).attr('clicked',0);
								  $(this).remove();
							  }
						});
					});
				  }
        },
				
				question: function(){
					
					var id = attrs.question;
					
					var isAnswer = false;
					
					if(attrs.answer&& attrs.answer.length>0){id = attrs.answer;isAnswer=true;}
					
					if(isAnswer && (attrs.reported>0 || attrs.approved>0 || attrs.shared>0)){
						
						var did = attrs.reported>0 ? 'reported' : false;
						
						if(!did){did = attrs.approved>0 ? 'approved' : false;}
						
						if(!did){did = 'shared';}
						
						panel.alert("You already "+did+" this answer."); return;
					}
					
					var action = attrs.todo;
					
					var following = attrs.following;
					if(action == 'follow'){
                        if (following == 'true'){
                            action = 'unfollow';
                        }
					}
					
					if(action == 'share' && isAnswer){
						
						action = 'share-answer';
					}
					
					if(action == 'share' && attrs.shared.length>0 && attrs.shared!="[]"){
						
						panel.alert("You already shared this question!");return;
					}
					
					if(action == 'endorse' && attrs.endorsed.length>0 && attrs.endorsed!="[]"){
						
						panel.alert("You already recommended this question!");return;
					}
					
					var object = {id:id, action:action};
					
                    if(attrs.answer){object.opinion = attrs.opinion;}
					
					panel.post('/community/action', object, function(response){
						
						if(response.Request == 'OK'){
							
							if(scope.questions){
								
								var index = attrs.index;
								
								if(action == 'follow'){
									
									scope.questions[index].followers.size = parseInt(scope.questions[index].followers.size) + 1;
									
									scope.questions[index].following = response.following;
                                    
                                    scope.questions[index].is_following = true;
								}
								else if(action == 'unfollow'){
									
									scope.questions[index].followers.size = parseInt(scope.questions[index].followers.size) - 1;
									
									delete scope.questions[index].following;
                                    
                                    scope.questions[index].is_following = false;
								}
								else if(action == 'endorse'){
									
									scope.questions[index].endorsements.size = parseInt(scope.questions[index].endorsements.size) + 1;
									
									scope.questions[index].endorsed = response.endorsement;
								}
								else if(action == 'share'){
									
									scope.questions[index].shares.size = parseInt(scope.questions[index].shares.size) + 1;
									
									scope.questions[index].shared = response.shared;
								}
								
								if(attrs.callback){(scope[attrs.callback])(index);}
							}
							else{
								
								if(action == 'follow'){
									
									scope.question.followers.size = parseInt(scope.question.followers.size) + 1;
									
									scope.question.following = response.following;
                                    
                                    scope.question.is_following = true;
								}
								else if(action == 'unfollow'){
									
									scope.question.followers.size = parseInt(scope.question.followers.size) - 1;
									
									delete scope.question.following;
                                    
                                    scope.question.is_following = false;
								}
								else if(action == 'endorse'){
									
									scope.question.endorsements.size = parseInt(scope.question.endorsements.size) + 1;
									
									scope.question.endorsed = response.endorsement;
								}
								else if(action == 'share'){
									
									scope.question.shares.size = parseInt(scope.question.shares.size) + 1;
									
									scope.question.shared = response.shared;
								}
								else if(action == 'approve'){
									
									scope.question.answers[attrs.index].approved = 1;
									
									scope.question.answers[attrs.index].approvals.size = 
										parseInt(scope.question.answers[attrs.index].approvals.size) + 1;
								}
								else if(action == 'share-answer'){
									
									scope.question.answers[attrs.index].shared = 1;
									
									scope.question.answers[attrs.index].shares.size = 
										parseInt(scope.question.answers[attrs.index].shares.size) + 1;
								}
							}
						}
					});
				},
				
				add_photo: function(){location.href = '/upload-business-photos/'+attrs.business;}
			}
			
			element.bind('click', function(){
				
        panel.fetch('/house/alive', function(response){
			    
					if(response.Request == 'OK'){
						
						(actions[attrs.action])();
					}   
					else if(panel.ERROR.login_required(response)){
				    panel.inpage_login(false, function(){
							(actions[attrs.action])();
						});
					}
				}, true);
				
			});
			/*
			
			if((typeof attrs.liked != 'undefined') && attrs.liked == 'true'){$(element).prop("disabled", true);}
			*/
        }
    };
	
}]);

ascoreDirectives.directive('sharethis', ['$timeout','$window', function($timeout,$window) {
  return {
      restrict: 'A',
      scope: {
        sharethis: '@sharethis'
      },
      replace: true,
      template:"",
      link: function(scope, element, attr) {
        
      }
    };
}]);

ascoreDirectives.directive('socialNetworking', ['panel','$window', function(panel, $window){
	
	return {
		
		restrict: 'A',
		
		link: function(scope, element, attrs){
			
			var platform = attrs.platform;
			
			var hooks = {
				
				facebook: function(){
         
				},
				
				twitter: function(){
					
					
				},
				
				instagram: function(){
					
					
				},
				
				tumblr: function(){
					
					
				},
				
				gplus: function(){

				},
				
				linkedin: function(){
					
					
				},
				
				mail: function(){
					
					
				},
				
				all: function(){
					
					
				},
				
                bookmark: function () {
                    panel.continueIfAccess(function () {
                        var data = {type: 'manage_bookmarks', 'action': 'add', business_id: attrs.business};

                        panel.post('/incoming', data, function (response) {

                            if (response.Request == 'OK') {

                                panel.alert("This business has been added to your bookmarks!");
                            }
                        });
                    });
                }
			}
			
			$(element).css({'cursor':'pointer'});
			
			element.bind('click', function(){
				
				(hooks[attrs.platform])();
			});
		}
	}
}]);


ascoreDirectives.directive('addBusinessPhotos', ['panel', function(panel){
	
  return {
	  
	restrict: 'A',
	
	link: function(scope, element, attr){
		
		var business_id = attr['business'];
		
		var upload = function(form){
			
			panel.fetch('/house/alive', function(response){
				
				if(response.Request == 'OK'){
					
					panel.upload('/incoming/upload', form, function(response){
				
						if(response.Request == 'OK'){
						
							panel.alert("You images have been uploaded successfully");
						}
					});
					
				}
				else if(panel.ERROR.login_required(response)){
							
					panel.inpage_login(false, function(){
						
						panel.upload('/incoming/upload', form, function(response){
				
							if(response.Request == 'OK'){
							
								panel.alert("You images have been uploaded successfully");
							}
						});
					});
				}
			});
		}
		
		element.bind('click', function(){
			
			var input = '<input type="file" name="files[]" multiple id="file-picker" />';
			
			$(input).appendTo('body').css({'display':'none'});
			
			$("#file-picker").click().on('change', function(){
			
				var files = this.files;
				
				var form = new FormData();
				
				for(var i in files){
				
					form.append('images[]', files[i]);
				}
				
				form.append('data', JSON.stringify({'business_id':business_id}));
				
				upload(form);
			});
		});
	}
  }
}]);


ascoreDirectives.directive('facebookLogin', ['panel', '$window', function(panel, $window){
	
	return {
		
		restrict: 'A',
		
		link: function(scope, element, attrs){
			
			var ascoreAPI = function(){
				
			console.log('Welcome!  Fetching your information.... ');
				
				FB.api('/me',{fields: 'id,name,email,picture'}, function(response){
		  	  console.log(JSON.stringify(response));
          panel.post('/access/facebok_auth', response, function(response){
            if(response.Request == "OK"){
              if(attrs.page == 'access'){
              $window.location.href = response.redirect;
              }else{
                var myEl = angular.element( document.getElementById('sign-in') );
				var __static_serv = panel.__staticserv();
                myEl.html('<ul class="session-menu"><li id="user-menu" ng-mouseover="toggleMenu(\'.user-dropdown-menu\')" ng-mouseout="toggleMenu(\'.user-dropdown-menu\')"><div style="display:inline-block;margin-top:-40px;text-align:center;"><a><img class="menu-profile-pic" ng-src="'+panel.__staticserv()+'/images/profile/'+response.profile_img+'"  src="'+panel.__staticserv()+'/images/profile/'+response.profile_img+'"/><span class="small menu-rating"><span>7.5</span> <img src="'+__static_serv+'/public/images/icons/ascore-check.png"/></span></a></div><div class="user-dropdown-menu hide"><a href="/user/{{session.username}}"><img class="menu-icon" src="'+__static_serv+'/public/images/icons/my-page.png"/> <span>My Page</span></a><a href="/user/{{session.username}}/about#abo"><img class="menu-icon" src="'+__static_serv+'/public/images/icons/profile.png"/> <span>Profile</span></a><a href="/user/{{session.username}}/settings#set"><img class="menu-icon" src="'+__static_serv+'/public/images/icons/preferences.png"/> <span>Preferences</span></a><a href="/access/logout"><img class="menu-icon" src="'+__static_serv+'/public/images/icons/sign_out2.png"/> <span>Sign out</span></a></div></li><li><span class="lcount label label-success">5</span><a href="#"><img class="rollover menu-icon" src="'+__static_serv+'/public/images/icons/kudos_icon.png"/><img class="xrollover menu-icon-hover" src="'+__static_serv+'/public/images/icons/kudos_icon_hover.png"/></a></li><li><span class="lcount label label-danger">5</span><a href="/messages"><img class="rollover menu-icon" src="'+__static_serv+'/public/images/icons/message.png"/><img class="xrollover menu-icon-hover" src="'+__static_serv+'/public/images/icons/message_hover.png"/></a></li><li><span class="lcount label label-danger">2</span><a href="#"><img class="rollover menu-icon" src="'+__static_serv+'/public/images/icons/questions.png"/><img class="xrollover menu-icon-hover" src="'+__static_serv+'/public/images/icons/questions_hover.png"/></a></li></ul>');
      					panel.inpage_login(true);
      					if(panel.inpage_callback.length == 1){
      						(panel.inpage_callback[0])();
      					}
              }
              
            }
  			  });
				});
			}
			
			var authorized;
			
			var statusChangeCallback = function(response){
				
				if (response.status === 'connected'){
					authorized = true;
				}
				else if(response.status === 'not_authorized' || response.status === 'unknown' || response.authResponse == 'undefined'){
					
					authorized = false;
				}
				else{
					
				}
			}

			var checkLoginState = function(){
				
				FB.getLoginStatus(function(response){
					
					statusChangeCallback(response);
				});
			}

			$window.fbAsyncInit = function(){
				
				FB.init({
					appId      : '1647159605547891',
					xfbml      : true,
					version    : 'v2.5'
				});
				
				FB.getLoginStatus(function(response){
					
					statusChangeCallback(response);
				});
			};
			
			element.bind('click', function(){
				
				
				
				if(!authorized){
							FB.login(function(response){
								tick = true;
					ascoreAPI();
							}, {scope: 'public_profile,email'});
						}else{
					ascoreAPI();
				}
			});
			
			(function(d, s, id){
					
				var js, fjs = d.getElementsByTagName(s)[0];
				
				if (d.getElementById(id)) return;
				
				js = d.createElement(s); js.id = id;
				
				js.src = "//connect.facebook.net/en_US/sdk.js";
				
				fjs.parentNode.insertBefore(js, fjs);
				
			}(document, 'script', 'facebook-jssdk'));
		}
	}
}]);

ascoreDirectives.directive('userCoordinates', ['panel', function(panel){
	
	return {
		
		restrict: 'A',
		
		link: function(){
			
			if (navigator.geolocation){
				
				navigator.geolocation.getCurrentPosition(function(position){
					
					var data = {type:'user_coordinates'};
					
					data.latitude = position.coords.latitude;
					
					data.longitude = position.coords.longitude;
					
					panel.post('/incoming', data, function(response){
				
						if(response.Request == 'OK'){
							
							$scope.clocker = response.clock;
						}
						else{
							
							if(response.error){
								
								panel.alert(response.error);
							}
						}
					});
				});
			}
		}
	}
}]);

ascoreDirectives.directive('googleplusLogin', ['panel', '$window', function(panel, $window){
	
	return {
		
		restrict: 'A',
		
		link: function(scope, element, attrs){
	    
			var googleUser = {};
      
			function attachSignin(element,attrs){
			//  console.log(element.id);
			auth2.attachClickHandler(element, {},
				function(googleUser) {
				  var picture = {'data':{'url':googleUser.getBasicProfile().getImageUrl()}};
				  var data = {email:googleUser.getBasicProfile().getEmail(), name:googleUser.getBasicProfile().getName(),picture:picture};
				  panel.post('/access/facebok_auth', data, function(response){
					  if(response.Request == "OK"){
					  if(attrs.page == 'access'){
					  $window.location.href = response.redirect;
					  }else{
						var myEl = angular.element( document.getElementById('sign-in') );
					myEl.html('<ul class="session-menu"><li id="user-menu" ng-mouseover="toggleMenu(\'.user-dropdown-menu\')" ng-mouseout="toggleMenu(\'.user-dropdown-menu\')"><div style="display:inline-block;margin-top:-40px;text-align:center;"><a><img class="menu-profile-pic" ng-src="'+panel.__staticserv()+'/images/profile/'+response.profile_img+'"  src="'+panel.__staticserv()+'/images/profile/'+response.profile_img+'"/><span class="small menu-rating"><span>7.5</span> <img src="'+panel.__staticserv()+'/public/images/icons/ascore-check.png"/></span></a></div><div class="user-dropdown-menu hide"><a href="/user/{{session.username}}"><img class="menu-icon" src="'+panel.__staticserv()+'/public/images/icons/my-page.png"/> <span>My Page</span></a><a href="/user/{{session.username}}/about#abo"><img class="menu-icon" src="'+panel.__staticserv()+'/public/images/icons/profile.png"/> <span>Profile</span></a><a href="/user/{{session.username}}/settings#set"><img class="menu-icon" src="'+panel.__staticserv()+'/public/images/icons/preferences.png"/> <span>Preferences</span></a><a href="/access/logout"><img class="menu-icon" src="'+panel.__staticserv()+'/public/images/icons/sign_out2.png"/> <span>Sign out</span></a></div></li><li><span class="lcount label label-success">5</span><a href="#"><img class="rollover menu-icon" src="'+panel.__staticserv()+'/public/images/icons/kudos_icon.png"/><img class="xrollover menu-icon-hover" src="'+panel.__staticserv()+'/public/images/icons/kudos_icon_hover.png"/></a></li><li><span class="lcount label label-danger">5</span><a href="/messages"><img class="rollover menu-icon" src="'+panel.__staticserv()+'/public/images/icons/message.png"/><img class="xrollover menu-icon-hover" src="'+panel.__staticserv()+'/public/images/icons/message_hover.png"/></a></li><li><span class="lcount label label-danger">2</span><a href="#"><img class="rollover menu-icon" src="'+panel.__staticserv()+'/public/images/icons/questions.png"/><img class="xrollover menu-icon-hover" src="'+panel.__staticserv()+'/public/images/icons/questions_hover.png"/></a></li></ul>');
								panel.inpage_login(true);
								if(panel.inpage_callback.length == 1){
									(panel.inpage_callback[0])();
								}
					  }
					  }
					  });
				}, function(error) {
				  console.log(JSON.stringify(error, undefined, 2));
				});
			}
     
			$window.init_google_login = function(){
			gapi.load('auth2', function(){
			  auth2 = gapi.auth2.init({
				client_id: '394354923156-o5t8rhrh3snhftdemvokfpac7mfbtcau.apps.googleusercontent.com',
				cookiepolicy: 'single_host_origin',
			  });
			  attachSignin(document.getElementById('customBtn'),attrs);
			});
			};
	  
			$(document).ready(function(){
				
				var s = document.createElement("script");

				s.src = "https://apis.google.com/js/api:client.js?onload=init_google_login"; $("html").append(s);
				
			});
		}
	}
}]);


function debounce(func, wait, immediate) {
	var timeout;
	return function() {
		var context = this,
			args = arguments;
		var later = function() {
			timeout = null;
			if (!immediate) func.apply(context, args);
		};
		var callNow = immediate && !timeout;
		clearTimeout(timeout);
		timeout = setTimeout(later, wait);
		if (callNow) func.apply(context, args);
	};
};
function randomString(len, an) {
	an = an && an.toLowerCase();
	var str = "",
		i = 0,
		min = an == "a" ? 10 : 0,
		max = an == "n" ? 10 : 62;
	for (; i++ < len;) {
		var r = Math.random() * (max - min) + min << 0;
		str += String.fromCharCode(r += r > 9 ? r < 36 ? 55 : 61 : 48);
	}
	return str;
}

/* The panel service was made part of root here */

ascoreApp.run(function($rootScope, panel){$rootScope.panel = panel;});